<?php
class SHomeController extends SettingsBaseControllers
{

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }



    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    public function accessRules()
    {
        return array(

            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('update','index'),
                'users' => array('admin','super_admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }


    public function actionIndex(){


        $this->breadcrumbs=array($this->module->id);

        $this->setPageTitle(Yii::app()->name.' - Settings');

        $model_general = $this->loadModel(1);

        $obj = new SettingsObj();

        $this->performAjaxValidation($model_general);

        if(isset($_POST['Settings']))
        {
            $postData = $_POST['Settings'];

            $model_general->attributes = $postData ;

            $model_general->timezone = $postData['timezone'];

            $model_general->pinned = $postData['pinned'];

            $model_general->start_date_time = $postData['start_date_time'];

            $model_general->end_date_time = $postData['end_date_time'];

            $model_general->gap_time = $postData['gap_time'];

            $model_general->how_many_posts = $postData['how_many_posts'];

            $model_general->direct_push = $postData['direct_push'];

            echo CJSON::encode(array('valid'=>$model_general->save()));

        }else{
            $this->render('index',array('general'=>$model_general,'obj'=>$obj));
        }

    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        $this->performAjaxValidation($model);

        if(isset($_POST['Settings']))
        {
                echo CJSON::encode(array('valid'=>$model->save()));
        }
            $this->render('index',array('general'=>$model));

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Settings the loaded model
     * @throws CHttpException
     */

    public function loadModel($id)
    {
        $model=Settings::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Settings $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='settings-form' )
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}