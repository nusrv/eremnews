<?php

class DayController extends SettingsBaseControllers
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('edit','delete','index'),
                'users' => array('admin','super_admin'),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
        $create=new DaySettings;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($create);

        if(isset($_POST['DaySettings'])){
            $create->attributes=$_POST['DaySettings'];
            if($create->save()){

            }
        }


		$model=new DaySettings('search');

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Day']))
			$model->attributes=$_GET['Day'];

		$this->render('admin',array(
			'model'=>$model,
			'create'=>$create,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return DaySettings the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=DaySettings::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param DaySettings $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Day-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    public function actionEdit(){
        if(isset($_POST)){
            if(isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk']) && isset($_POST['scenario'])){
                $model = $this->loadModel($_POST['pk']);

                if(!empty($_POST['name'])){
                    $name = $_POST['name'];
                    $value = $_POST['value'];
                    $model->$name = $value;
                    if($model->save())
                        echo true;
                    else
                        echo false;
                }
            }

        }
    }
}
