<?php
/* @var $this SHomeController */
/* @var $general Settings */
/* @var $obj SettingsObj */
?>
<style>
	.settings-page-title{
		margin-top: 0px;
	}
	.Scheduled-title{
		background-color: #f1efef;
		padding: 10px 0px 10px 15px ;
	}
	.Scheduled-body{
		border: 1px solid #b1b1b1;
		margin-bottom: 20px;
	}
</style>
<section class="content">

	<div class="row">


			<div class="col-sm-12">

				<div class="box box-warning loading-page">
					<?php
					foreach(Yii::app()->user->getFlashes() as $key => $message) {
						echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
					}
					?>
					<div class="box-header with-border">
						<div class="col-sm-12 msg"></div>

						<div class="col-sm-5">

							<h4 class="settings-page-title">

								General Settings

							</h4><!-- end settings-page-title-->

						</div><!-- end col-sm-5-->

						<div class="col-sm-3 pull-right">

							<?php echo Yii::app()->params['statement']['previousPage']; ?>



							<?php echo TbHtml::link('custom settings',$this->createUrl('/settings/custom'),array('class'=>'btn btn-success btn-sm')) ?>

						</div><!-- end col-sm-3 pull-right-->

					</div><!-- end box-header with-border-->

					<div class="box-body">
						<div class="col-sm-12">
							<?php echo $this->renderPartial('general_settings',array('model'=>$general))?>
						</div>

					</div><!-- end box-body-->



				</div><!-- end box box-warning-->

			</div><!-- end col-sm-12-->



		<div class="col-sm-12">

				<div class="box box-warning">

					<div class="box-header with-border">


						<div class="col-sm-5">

							<h4 class="settings-page-title">

								Settings object

							</h4><!-- end settings-page-title-->

						</div><!-- end col-sm-5-->


					</div><!-- end box-header with-border-->

					<div class="box-body">
						<div class="col-sm-12">
							<div class="col-sm-12">
								<?php
								$this->widget('booster.widgets.TbGridView', array(
									'id'=>'obj-grid',
									'type' => 'striped bordered condensed',
									'template' => '{pager}{items}{summary}{pager}',
									'enablePagination' => true,
									'pager' => array(
										'class' => 'booster.widgets.TbPager',
										'nextPageLabel' => 'Next',
										'prevPageLabel' => 'Previous',
										'htmlOptions' => array('class' => 'pull-right')
									),
									'htmlOptions' => array('class' => 'table-responsive'),
									'filter' => $obj,
									'dataProvider' => $obj->search(),
									'columns' => array(
										'id',
										'name',
										array(
											'class' => 'booster.widgets.TbButtonColumn',
											'header' => 'Options',
											'template' => '{update}{activate}{deactivate}',
											'buttons' => array(
												'update' => array(
													'label' => 'Update',
													'icon' => 'fa fa-pencil-square-o',
													'url'=>'CHtml::normalizeUrl(array("/settings/custom/update/id/".$data->id))'
												),
												/*'active' => array(
													'label' => 'Delete',
													'visible' => '$data->active==1?true:false',
													'icon' => 'fa fa-times',
													'url'=>'CHtml::normalizeUrl(array("/settings/custom/delete/id/".$data->id))'
												),
												'nonactive' => array(
													'label' => 'Delete',
													'visible' => '$data->active==0?true:false',
													'icon' => 'fa fa-times',
													'url'=>'CHtml::normalizeUrl(array("/settings/custom/delete/id/".$data->id))'
												),*/

												'activate' => array(
													'label' => 'Deactivate',
													'url' => 'Yii::app()->createUrl("/settings/custom/active", array("id"=>$data->id))',
													'icon' => 'fa fa-toggle-off',
													'visible' => '$data->active == 1',
													'click' => "function(){

				$.fn.yiiGridView.update('obj-grid', {  //change my-grid to your grid's name
				type:'POST',
				url:$(this).attr('href'),
				success:function(data) {
				$.fn.yiiGridView.update('obj-grid');}});return false;}",
												),
												'deactivate' => array(
													'label' => 'Activate',
													'url' => 'Yii::app()->createUrl("/settings/custom/active", array("id"=>$data->id))',
													'icon' => 'fa fa-toggle-on',
													'visible' => '$data->active == 0',
													'click' => "function(){
				$.fn.yiiGridView.update('obj-grid', {  //change my-grid to your grid's name
				type:'POST',
				url:$(this).attr('href'),
				success:function(data) {
				$.fn.yiiGridView.update('obj-grid');}});return false;}"
												),
											)
										),
									)));
								?>

							</div>
						</div>

					</div><!-- end box-body-->



				</div><!-- end box box-warning-->

			</div><!-- end col-sm-12-->



	</div><!-- end row -->



</section><!-- end content -->
