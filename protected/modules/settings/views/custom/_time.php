<div class="col-sm-12">

    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'start_time',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-12',

                ),
                'labelOptions' => array(
                    'class' => 'col-sm-12',
                ),
                'widgetOptions'=>array(
                    'htmlOptions'=>array(
                        'autocomplete'=>'off',
                        'name'=>'CustomSettingsForm[start_time][]'
                    )
                )

            )
        );?>
    </div>
    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'end_time',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-12',
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-12',
                ),
                'widgetOptions'=>array(
                    'htmlOptions'=>array(
                        'autocomplete'=>'off',
                        'name'=>'CustomSettingsForm[end_time][]'
                    )
                )
            )
        );?>
    </div>
    <div class="col-sm-2">

        <?php echo $form->textFieldGroup(
            $model,
            'direct_push_start_time',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-12',
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-12',
                ),
                'widgetOptions'=>array(
                    'htmlOptions'=>array(
                        'autocomplete'=>'off',
                        'name'=>'CustomSettingsForm[direct_push_start_time][]'
                    )
                ),
                'append'=>$form->checkBox($model,'is_direct_push',array('name'=>'CustomSettingsForm[is_direct_push][]'))
            )
        );?>
    </div>

    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'direct_push_end_time',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-12',
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-12',
                ),
                'widgetOptions'=>array(
                    'htmlOptions'=>array(
                        'autocomplete'=>'off',
                        'name'=>'CustomSettingsForm[direct_push_end_time][]'
                    )
                )
            )
        );?>
    </div>
    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'gap_time',
            array(

                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-12',
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-12',
                ),
                'widgetOptions'=>array(
                    'data'=>Settings::model()->gap_time(),
                    'empty'=>'Choose One',
                    'htmlOptions'=>array(

                        'name'=>'CustomSettingsForm[gap_time][]'
                    )
                )
            )
        );?>
    </div>
    <div class="col-sm-2">
        <?php echo TbHtml::button('+',array('class'=>'btn btn-success btn-sm margin_pules','name'=>'add_btn','id'=>'add_btn'))?>
    </div>
</div>