<?php
class ThematicPostBaseControllers extends BaseController{

    public $details;
    public $category;
    public $category_id;
    public $sub_category_id;
    public $model;
    public $trim_hash;
    public $hash_tag;
    public $platform;
    public $parent = null;
    public $creator;
    public $sub_category;
    public $source;
    public $news;
    public $PostQueue;
    public $shorten_url;
    public $new_category;



    public function get_template($platform,$category){

        $temp  = PostTemplate::model()->findByAttributes(array(
            'platform_id'=>$platform->id,
            'catgory_id'=>$category,
        ),
            array(
                'condition'=>'`type` != "video"',
                'order'=>'rand()',
            ));
        if(!empty($temp))
            return $temp;
        $cond = new CDbCriteria();
        $cond->order = 'RAND()';
        $cond->condition='( ( platform_id = '.$platform->id.' or platform_id is NUll ) and type != "video" and  catgory_id is NULL )';
        $temp  = PostTemplate::model()->find($cond);

        if(!empty($temp))
            return $temp;

        return Yii::app()->params['templates'];
    }



    public function get_date($date){

        return date('Y-m-d H:i:s',strtotime($date));

    }


    public function generator_info($info){

        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index=>$item) {
            $this->trim_hash[$index]=" #".str_replace(" ", "_",trim($item->title)).' ';
            $this->hash_tag[$index]=' '.trim($item->title).' ';
        }

        $this->parent = null;

        if(isset($this->category->title))
            $this->category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_',$this->category->title));

        if(isset($this->sub_category->title))
            $this->sub_category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_', $this->sub_category->title));

        $this->source = Yii::app()->params['source'].' : ';

        $array = array();

            foreach (Platform::model()->findAllByAttributes(array('deleted' => 0)) as $attribute) {
                $this->platform = $attribute;
               $array[strtolower($this->platform->title)] =  $this->post_info($info);
            }
        $array['info'] = $info;

        return $array;

    }

    public function post_info($info){

        $media= null;

        $temp = $this->get_template($this->platform,isset($this->category_id)?$this->category_id:null);

        $title = $info['title'];

        $description = $info['description'];

        if($this->platform->title != 'Instagram'){
            $title = str_replace($this->hash_tag, $this->trim_hash,$title);
            $description = str_replace($this->hash_tag, $this->trim_hash,$description);
        }

        $title =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);
        $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

        $description =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);
        $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);



        $full_creator = $this->source.$info['creator'];
        if(!empty(strpos($info['creator'],':')))
            $full_creator = $info['creator'];

        $cre= $this->platform->title == 'Twitter'?($this->creator?$full_creator:null):$full_creator;


        $text = str_replace(
            array('[title]','[description]','[short_link]','[author]'),
            array($title,$description,$this->shorten_url,$cre),
            $temp['text']
        );

        if($this->platform->title == 'Facebook' or $this->platform->title == 'Twitter'){

            $text = str_replace('# ', '#', $text);

            $found = true;

            preg_match_all("/#([^\s]+)/", $text, $matches);

            if (isset($matches[0])) {

                $matches[0] = array_reverse($matches[0]);
                $count = 0;
                foreach ($matches[0] as $hashtag) {


                    if(strpos($text,'[section]')) {
                        if ($count >= 1) {
                            $text = str_ireplace($hashtag, str_ireplace('_', ' ', str_ireplace('#', '', $hashtag)), $text);
                            break;
                        }
                    }else{
                        if ($count >= 2) {
                            $found = false;
                            $text = str_ireplace($hashtag, str_ireplace('_', ' ', str_ireplace('#', '', $hashtag)), $text);
                        }
                    }
                    $count++;
                }
                $found =true;
                if ($count >= 2)
                    $found = false;
            }

            if($found)
                $text = str_replace(
                    array('[section]', '[sub_section]'),
                    array(isset($this->category)?$this->category:null, isset($this->sub_category)?$this->sub_category:null),
                    $text
                );
            else
                $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);

        }elseif($this->platform->title=='Instagram')
            $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', ''), str_replace('# ', '#', $text));

        if($this->platform->title == 'Twitter'){
            $text_twitter =$text;
            if($temp['type'] == 'Preview')
                if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                    $text =$text.PHP_EOL.$this->shorten_url;
            if(!$this->getTweetLength($text_twitter,$temp['type'] == 'Image'?true:false,$temp['type'] == 'Video'?true:false) > 141) {
                $text = $text_twitter.PHP_EOL;
                if($temp['type'] == 'Preview')
                    if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                        $text .=PHP_EOL.$this->shorten_url;
            }
        }

        $text = str_replace('# #', '#', $text);
        $text = str_replace('##', '#', $text);
        $text = str_replace('&#8220;', '', $text);
        $text = str_replace('ـــــ', ' ', $text);
        $text = str_replace('&#8221;', '', $text);
        $text = str_replace('&#8230;', '', $text);
        $text = str_replace('#8211;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&nbsp;', '', $text);
        $text = str_replace('&#160;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&ndash;', '', $text);
        $text = str_replace('  ', ' ', $text);
      //  if($this->platform->title != 'Instagram'){
            $text =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
            $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);
      //  }
        $type = $temp['type'];
        $post = $text;
        $platform =$this->platform->id;
        return array('type'=>$type,'post'=>$post,'platform'=>$platform,'temp_id'=>$temp['id']);
    }

    public function shorten_point($input, $point = '.') {
        $input_ex = explode($point,$input);
        $data = array_chunk($input_ex,2);
        if(is_array($data)){

            return (implode('. '.PHP_EOL,$data[0]));
        }
        return $input;
    }





}