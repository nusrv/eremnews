<?php
/* @var $this DefaultController */
/* @var $total_posts DefaultController */
/* @var $model PostQueue */
Yii::app()->clientScript->registerScript('search', "
$('#search-form').change(function(){
if($('#PostQueue_schedule_date').val() != '' ){
    $(this).submit();
    }if($('#PostQueue_to').val() != '' ){
    $(this).submit();
    }
});
$('#PostQueue_schedule_date').attr('readonly','readonly');
$('#PostQueue_to').attr('readonly','readonly');
/*$('#yw28').contentChanged(function(){
});*/
");
?>
<?php

?>
<script>

    $(document).ready(function(){
        $('.thematics').click(function(){
            if($(this).hasClass('fa-plus')){
                $('.thematic').show("slow");
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
            }else if($(this).hasClass('fa-minus')){
                $('.thematic').hide("slow");
                $(this).addClass('fa-plus');
                $(this).removeClass('fa-minus');
            }
        });
        $('.nonthematics').click(function(){
            if($(this).hasClass('fa-plus')){
                $('.Nonthematic').show("slow");
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
            }else if($(this).hasClass('fa-minus')){
                $('.Nonthematic').hide("slow");
                $(this).addClass('fa-plus');
                $(this).removeClass('fa-minus');
            }
        });
        $('.all-automated').click(function(){
            if($(this).hasClass('fa-plus')){
                $('.automated').show("slow");
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
            }else if($(this).hasClass('fa-minus')){
                $('.automated').hide("slow");
                $(this).addClass('fa-plus');
                $(this).removeClass('fa-minus');
            }
        });
        $('.all-cloned').click(function(){
            if($(this).hasClass('fa-plus')){
                $('.cloned').show("slow");
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
            }else if($(this).hasClass('fa-minus')){
                $('.cloned').hide("slow");
                $(this).addClass('fa-plus');
                $(this).removeClass('fa-minus');
            }
        });
        $('.collapse-all').click(function(){
            if($(this).hasClass('fa-plus')){
                $('.automated,.Nonthematic,.thematic').show("slow");
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
                $('.all-automated,.nonthematics,.thematics').removeClass('fa-plus');
                $('.all-automated,.nonthematics,.thematics').addClass('fa-minus');

            }else if($(this).hasClass('fa-minus')){
                $('.automated,.Nonthematic,.thematic').hide("slow");
                $(this).addClass('fa-plus');
                $(this).removeClass('fa-minus');
                $('.all-automated,.nonthematics,.thematics').addClass('fa-plus');
                $('.all-automated,.nonthematics,.thematics').removeClass('fa-minus');

            }
        });
    })
</script>
<style>
    .thematic,.Nonthematic,.automated,.cloned{
        color:black;
        display: none;
        background-color: #d7edf4;
    }
    .fa{
        cursor: pointer;
    }
    td,th{
      /*  border-right:1px solid black;*/
        width:20%;
        overflow: auto;
        text-align: center;
    }
  
    h5{
        border-bottom:1px solid #229bc3;
    }
    td h6{
        border-bottom: 1px dotted #229bc3;
        max-width:90px;
        margin-left:15%;
        font-size:14px;
    }
    .collapse-all{
        margin-left:50px;
    }
    .column-color{
        background-color: #ADD8E6
    }
    td {
        height: 70px;
    }
</style>

    <div class="row">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Pushed posts report</h3>

                <br /><br />
            <div class="container-fluid">
                <div class="table-responsive">
                    <i class="fa fa-plus collapse-all" data-toggle="tooltip" title="Collapse all"></i>&nbsp;&nbsp;&nbsp;Collapse all
                    <table class="table">

                        <thead>
                        <tr class="heads">
                            <th><h5><b>Total/Platforms</b></h5></th>
                            <th class="column-color"><h5><strong>Facebook</strong></h5></th>
                            <th><h5><strong>Twitter</strong></h5></th>
                            <th class="column-color"><h5><strong>Instagram</strong></h5></th>
                            <th><h5><strong>Total</strong></h5></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="bg-lightgreen">
                            <?php
                            $total_posts_summed_thematic = $this->array_sum_generated($total_posts,'thematic');
                            $total_non_thematic = $this->array_sum_generated($total_posts,'manual');
                            $total_auto_posts = $this->array_sum_generated($total_posts,'auto');
                            $total_cloned_posts = $this->array_sum_generated($total_posts,'clone');
                            ?>
                            <td><h5><strong><i class="fa fa-plus thematics"></i>&nbsp;&nbsp;&nbsp;Thematic</strong></h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts_summed_thematic['thematic']['facebook']['total']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['twitter']['total']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts_summed_thematic['thematic']['instagram']['total']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['total']); ?></h6></td>
                        </tr>
                        <tr class="thematic">
                            <td><h5>Preview</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts_summed_thematic['thematic']['facebook']['preview']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['twitter']['preview']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts_summed_thematic['thematic']['instagram']['preview']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['facebook']['preview']+$total_posts_summed_thematic['thematic']['twitter']['preview']+$total_posts_summed_thematic['thematic']['instagram']['preview']) ?></h6></td>
                        </tr>
                        <tr class="thematic">
                            <td><h5>Image</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts_summed_thematic['thematic']['facebook']['image']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['twitter']['image']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts_summed_thematic['thematic']['instagram']['image']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['facebook']['image']+$total_posts_summed_thematic['thematic']['twitter']['image']+$total_posts_summed_thematic['thematic']['instagram']['image']) ?></h6></td>
                        </tr>
                        <tr class="thematic">
                            <td><h5>Text</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts_summed_thematic['thematic']['facebook']['text']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['twitter']['text']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts_summed_thematic['thematic']['instagram']['text']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['facebook']['text']+$total_posts_summed_thematic['thematic']['twitter']['text']+$total_posts_summed_thematic['thematic']['instagram']['text']) ?></h6></td>
                        </tr>
                        <tr class="thematic">
                            <td><h5>Video</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts_summed_thematic['thematic']['facebook']['video']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['twitter']['video']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_posts_summed_thematic['thematic']['instagram']['video']) ?></h6></td>
                            <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['facebook']['video']+$total_posts_summed_thematic['thematic']['twitter']['video']+$total_posts_summed_thematic['thematic']['instagram']['video']) ?></h6></td>
                        </tr>
                        <tr>
                            <td><h5><strong><i class="fa fa-plus nonthematics"></i>&nbsp;&nbsp;&nbsp;Non-Thematic</strong></h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_non_thematic['manual']['facebook']['total']) ?></h6></td>
                            <td><h6><?php echo number_format($total_non_thematic['manual']['twitter']['total']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_non_thematic['manual']['instagram']['total']) ?></h6></td>
                            <td><h6><?php echo number_format($total_non_thematic['manual']['total']);?></h6></td>
                        </tr>
                        <tr class="Nonthematic">
                            <td><h5>Preview</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_non_thematic['manual']['facebook']['preview']) ?></h6></td>
                            <td><h6><?php echo number_format($total_non_thematic['manual']['twitter']['preview']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_non_thematic['manual']['instagram']['preview']) ?></h6></td>
                            <td><h6><?php echo number_format($total_non_thematic['manual']['facebook']['preview']+$total_non_thematic['manual']['twitter']['preview']+$total_non_thematic['manual']['instagram']['preview']) ?></h6></td>
                        </tr>
                        <tr class="Nonthematic">
                            <td><h5>Image</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_non_thematic['manual']['facebook']['image']) ?></h6></td>
                            <td><h6><?php echo number_format($total_non_thematic['manual']['twitter']['image']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_non_thematic['manual']['instagram']['image']) ?></h6></td>
                            <td><h6><?php echo number_format($total_non_thematic['manual']['facebook']['image']+$total_non_thematic['manual']['twitter']['image']+$total_non_thematic['manual']['instagram']['image']) ?></h6></td>
                        </tr>
                        <tr class="Nonthematic">
                            <td><h5>Text</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_non_thematic['manual']['facebook']['text']) ?></h6></td>
                            <td><h6><?php echo number_format($total_non_thematic['manual']['twitter']['text']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_non_thematic['manual']['instagram']['text']) ?></h6></td>
                            <td><h6><?php echo number_format($total_non_thematic['manual']['facebook']['text']+$total_non_thematic['manual']['twitter']['text']+$total_non_thematic['manual']['instagram']['text']) ?></h6></td>
                        </tr>
                        <tr class="Nonthematic">
                            <td><h5>Video</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_non_thematic['manual']['facebook']['video']) ?></h6></td>
                            <td><h6><?php echo number_format($total_non_thematic['manual']['twitter']['video']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_non_thematic['manual']['instagram']['video']) ?></h6></td>
                            <td><h6><?php echo number_format($total_non_thematic['manual']['facebook']['video']+$total_non_thematic['manual']['twitter']['video']+$total_non_thematic['manual']['instagram']['video']) ?></h6></td>
                        </tr>
                        <tr>
                        <tr>
                            <td><h5><strong><i class="fa fa-plus all-automated"></i>&nbsp;&nbsp;&nbsp;Automated posts</strong></h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_auto_posts['auto']['facebook']['total']) ?></h6></td>
                            <td><h6><?php echo number_format($total_auto_posts['auto']['twitter']['total']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_auto_posts['auto']['instagram']['total']) ?></h6></td>
                            <td><h6><?php echo number_format($total_auto_posts['auto']['total']);?></h6></td>
                        </tr>
                        <tr class="automated">
                            <td><h5>Preview</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_auto_posts['auto']['facebook']['preview']) ?></h6></td>
                            <td><h6><?php echo number_format($total_auto_posts['auto']['twitter']['preview']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_auto_posts['auto']['instagram']['preview']) ?></h6></td>
                            <td><h6><?php echo number_format($total_auto_posts['auto']['facebook']['preview']+$total_auto_posts['auto']['twitter']['preview']+$total_auto_posts['auto']['instagram']['preview']) ?></h6></td>
                        </tr>
                        <tr class="automated">
                            <td><h5>Image</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_auto_posts['auto']['facebook']['image']) ?></h6></td>
                            <td><h6><?php echo number_format($total_auto_posts['auto']['twitter']['image']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_auto_posts['auto']['instagram']['image']) ?></h6></td>
                            <td><h6><?php echo number_format($total_auto_posts['auto']['facebook']['image']+$total_auto_posts['auto']['twitter']['image']+$total_auto_posts['auto']['instagram']['image']) ?></h6></td>
                        </tr>
                        <tr class="automated">
                            <td><h5>Text</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_auto_posts['auto']['facebook']['text']) ?></h6></td>
                            <td><h6><?php echo number_format($total_auto_posts['auto']['twitter']['text']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_auto_posts['auto']['instagram']['text']) ?></h6></td>
                            <td><h6><?php echo number_format($total_auto_posts['auto']['facebook']['text']+$total_auto_posts['auto']['twitter']['text']+$total_auto_posts['auto']['instagram']['text']) ?></h6></td>
                        </tr>
                        <tr class="automated">
                            <td><h5>Video</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_auto_posts['auto']['facebook']['video']) ?></h6></td>
                            <td><h6><?php echo number_format($total_auto_posts['auto']['twitter']['video']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_auto_posts['auto']['instagram']['video']) ?></h6></td>
                            <td><h6><?php echo number_format($total_auto_posts['auto']['facebook']['video']+$total_auto_posts['auto']['twitter']['video']+$total_auto_posts['auto']['instagram']['video']) ?></h6></td>
                        </tr>
                        <tr>
                            <td><h5><strong><i class="fa fa-plus all-cloned"></i>&nbsp;&nbsp;&nbsp;Repushed posts</strong></h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['facebook']['total']) ?></h6></td>
                            <td><h6><?php echo number_format($total_cloned_posts['clone']['twitter']['total']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['instagram']['total']) ?></h6></td>
                            <td><h6><?php echo number_format($total_cloned_posts['clone']['total']);?></h6></td>
                        </tr>
                        <tr class="cloned">
                            <td><h5>Preview</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['facebook']['preview']) ?></h6></td>
                            <td><h6><?php echo number_format($total_cloned_posts['clone']['twitter']['preview']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['instagram']['preview']) ?></h6></td>
                            <td><h6><?php echo number_format($total_cloned_posts['clone']['facebook']['preview']+$total_cloned_posts['clone']['twitter']['preview']+$total_cloned_posts['clone']['instagram']['preview']) ?></h6></td>
                        </tr>
                        <tr class="cloned">
                            <td><h5>Image</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['facebook']['image']) ?></h6></td>
                            <td><h6><?php echo number_format($total_cloned_posts['clone']['twitter']['image']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['instagram']['image']) ?></h6></td>
                            <td><h6><?php echo number_format($total_cloned_posts['clone']['facebook']['image']+$total_cloned_posts['clone']['twitter']['image']+$total_cloned_posts['clone']['instagram']['image']) ?></h6></td>
                        </tr>
                        <tr class="cloned">
                            <td><h5>Text</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['facebook']['text']) ?></h6></td>
                            <td><h6><?php echo number_format($total_cloned_posts['clone']['twitter']['text']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['instagram']['text']) ?></h6></td>
                            <td><h6><?php echo number_format($total_cloned_posts['clone']['facebook']['text']+$total_cloned_posts['clone']['twitter']['text']+$total_cloned_posts['clone']['instagram']['text']) ?></h6></td>
                        </tr>
                        <tr class="cloned">
                            <td><h5>Video</h5></td>
                            <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['facebook']['video']) ?></h6></td>
                            <td><h6><?php echo number_format($total_cloned_posts['clone']['twitter']['video']) ?></h6></td>
                            <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['instagram']['video']) ?></h6></td>
                            <td><h6><?php echo number_format($total_cloned_posts['clone']['facebook']['video']+$total_cloned_posts['clone']['twitter']['video']+$total_cloned_posts['clone']['instagram']['video']) ?></h6></td>
                        </tr>
                        <tr>
                  <tr>
                      <td><h5><strong>All posts</strong></h5></td>
                      <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['facebook']['total'] + $total_auto_posts['auto']['facebook']['total'] + $total_non_thematic['manual']['facebook']['total'] + $total_posts_summed_thematic['thematic']['facebook']['total']) ;?></h6></td>
                      <td><h6><?php echo number_format($total_cloned_posts['clone']['twitter']['total'] + $total_auto_posts['auto']['twitter']['total'] + $total_non_thematic['manual']['twitter']['total'] + $total_posts_summed_thematic['thematic']['twitter']['total']) ;?></h6></td>
                      <td class="column-color"><h6><?php echo number_format($total_cloned_posts['clone']['instagram']['total'] + $total_auto_posts['auto']['instagram']['total'] + $total_non_thematic['manual']['instagram']['total'] + $total_posts_summed_thematic['thematic']['instagram']['total']) ;?></h6></td>
                      <td><h6><?php echo number_format($total_posts_summed_thematic['thematic']['total'] + $total_non_thematic['manual']['total'] + $total_auto_posts['auto']['total']+$total_cloned_posts['clone']['total']) ;?></h6></td>
                  </tr>
                        </tbody>
                    </table>
                </div>
            </div>
</div>
            </div>
    </div>

