<?php
/* @var $this FacebookAccountController */
/* @var $model FacebookAccount */

$this->breadcrumbs=array(
	'Facebook Accounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FacebookAccount', 'url'=>array('index')),
	array('label'=>'Create FacebookAccount', 'url'=>array('create')),
	array('label'=>'Update FacebookAccount', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FacebookAccount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FacebookAccount', 'url'=>array('admin')),
);
?>

<h1>View FacebookAccount #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'link_page',
		'page_id',
		'app_id',
		'secret',
		'token',
		'is_general',
	),
)); ?>
