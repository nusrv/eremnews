<?php
/* @var $this FacebookAccountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Facebook Accounts',
);

$this->menu=array(
	array('label'=>'Create FacebookAccount', 'url'=>array('create')),
	array('label'=>'Manage FacebookAccount', 'url'=>array('admin')),
);
?>

<h1>Facebook Accounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
