<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class SearchForm extends CFormModel
{
	public $from;
	public $to;


	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
            array('from,to', 'required'),
			array('from,to', 'type','type' => 'date','dateFormat' => 'yyyy-MM-dd'),
			array('from,to', 'required','on'=>'search'),
			array('to', 'check_dates','on'=>'clear_queue'),
			array('from', 'check_dates','on'=>'clear_queue'),
			array('from,to', 'safe'),
			
		);
	}

	public function check_dates($attribute){

		if(!empty($this->from) && !empty($this->to))
		if ((strtotime($this->from)) > (strtotime($this->to))){
			$this->addError($attribute, 'Please check between dates '.$attribute);
			return false;
		}
		return true;
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'from' => Yii::t('app','From Date'),
			'to'   => Yii::t('app','To Date'),
		);
	}


}
