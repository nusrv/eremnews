<?php

/**
 * This is the model class for table "archives_news".
 *
 * The followings are the available columns in table 'archives_news':
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $link
 * @property string $link_md5
 * @property string $description
 * @property string $shorten_url
 * @property string $publishing_date
 * @property integer $is_archives
 * @property string $created_at
 */
class ArchivesNews extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'archives_news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, title, link, link_md5, description, shorten_url, publishing_date, created_at', 'required'),
			array('category_id, is_archives', 'numerical', 'integerOnly'=>true),
			array('title, description, shorten_url', 'length', 'max'=>255),
			array('link', 'length', 'max'=>1000),
			array('link_md5', 'length', 'max'=>225),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_id, title, link, link_md5, description, shorten_url, publishing_date, is_archives, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'title' => 'Title',
			'link' => 'Link',
			'link_md5' => 'Link Md5',
			'description' => 'Description',
			'shorten_url' => 'Shorten Url',
			'publishing_date' => 'Publishing Date',
			'is_archives' => 'Is Archives',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('link_md5',$this->link_md5,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('shorten_url',$this->shorten_url,true);
		$criteria->compare('publishing_date',$this->publishing_date,true);
		$criteria->compare('is_archives',$this->is_archives);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArchivesNews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
