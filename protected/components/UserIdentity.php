<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    private $_id;
    public $user;
    private $full_user;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $user = User::model()->findByAttributes(array('username' => $this->username,'password' => md5($this->password),'deleted'=>'0','active'=> '1'));

        if(empty($user)){
            $user = User::model()->findByAttributes(array('email' => $this->username,'password' => md5($this->password),'deleted'=>'0','active'=> '1'));

        }

        if($user){
            $this->setState('user_name',$user->username);
            $this->_id  = $user->id ;
            $this->user = $user ;


            $this->username = $user->role;
            $this->setState('name',$user->name);
            $this->setState('type',$user->role);
            $this->setState('isAdmin',$user->role=='admin');
            $this->setState('admin_sys',($user->role=='admin' or $user->role=='super_admin') and $user->username == 'admin');

            $this->errorCode=self::ERROR_NONE;
        }else{
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }

		return !$this->errorCode;
	}

    public function getId()
    {
        return $this->_id;
    }
    public function isFullUser()
    {
        return $this->full_user;
    }


    public function getUser()
    {
        return $this->user;
    }

    public function getImage(){

    }
}