<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $models MediaNews */

$this->pageTitle = "News | View";


$this->breadcrumbs = array(
    'News' => array('admin'),
    $model->id,
);


?>
<script type="text/javascript">
  $(document).ready(function(){
      $('img').click(function(){
          view($(this).attr('src'));
      })


      function view(small){
          var w =640;
          var h = 310;
          var left = (screen.width/2)-(w/2);
          var top = (screen.height/2)-(h/2);

           viewwin = window.open(small,'viewwin', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
      }
  })
</script>
<style>
    img:hover{
        cursor: pointer;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">

                    <div class="col-md-12 pull-right" style="text-align: left;">
                        <?php echo Yii::app()->params['statement']['previousPage']; ?>

                    </div>
                </div>
                <div class="box-body">
                    <?php $this->widget('booster.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            array(
                                'name' => 'title',
                                'type' => 'raw',
                                'value' => CHtml::link($model->title, $model->shorten_url, array('target' => '_blank')),
                            ),
                            'description',
                            'schedule_date',


                        ),
                    )); ?>

                </div>
            </div>
        </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="col-md-11"><h2>Media</h2></div>


                </div>
                <div class="box-body">
                    <?PHP
                    $this->widget('booster.widgets.TbGridView', array(

                        'type' => 'striped bordered condensed',
                        'template' => '{pager}{items}{summary}{pager}',
                        'enablePagination' => true,
                        'pager' => array(
                            'class' => 'booster.widgets.TbPager',

                            'htmlOptions' => array(
                                'class' => 'pull-right'
                            )
                        ),
                        'htmlOptions' => array(
                            'class' => 'table-responsive'
                        ),
                        'dataProvider' => $media_model->search(),
                        'filter' => $media_model,
                        'columns' => array(

                            array(

                                'name' => 'type',
                                'type' => 'html',
                                'value' => $media_model->type,
                                'header' => 'Type',


                            ),
                            array(
                                'name' => 'media',

                                'type' => 'html',
                                'value' => function ($media_model) {

                                    if ($media_model->type == 'video') {
                                        echo '<video width="400" controls class="col-md-offset-2"><source src="' . $media_model->media . '" type="video/mp4"><source src="' . $media_model->media . '" type="video/ogg">
</video>';
                                    } elseif ($media_model->type == 'image' || $media_model->type == 'gallery') {

                                        echo CHtml::image($media_model->media, 'image', array('class' => 'col-md-offset-2', 'style' => 'max-width:300px;max-height:250px;'));
                                    }
                                },
                                'header' => 'Media',
                            ),


                        ))); ?>
                </div>
            </div>
        </div>

    </div>
</section>









