<?php
/* @var $this FillerDataController */
/* @var $model FillerData */

$this->pageTitle = "Filler data | Admin";

$this->breadcrumbs=array(
	'Filler Data'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FillerData', 'url'=>array('index')),
	array('label'=>'Create FillerData', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#filler-data-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerCss('mycss', '


.fa-facebook{

color:#3b5998;
}
.fa-twitter {
    color:#00aced;
}
.fa-instagram{
color: #517FA6;
}

');
?>
<style>
	.showhideitem{
		display: block;
	}
</style>

<style>
	.checkbox {
		display: block;
		min-height: 0px;
		margin-top: 4px;
		margin-bottom: 0px;
		padding-left: 17px;
	}
	.input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
		margin-top: 0;
	}
	.radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
		float: left;
		margin-left: -16px;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('img').click(function(){
			view($(this).attr('src'));
		});
		function view(small){
			var w =1000;
			var h = 500;
			var left = (screen.width/2)-(w/2);
			var top = (screen.height/2)-(h/2);
			viewwin = window.open(small,'viewwin', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
		}
	})
</script>
<style>
	img:hover{
		cursor: pointer;
	}
</style>
<script>
	$( document ).ready(function() {
		$('#FillerData_publish_time').datetimepicker({
			datepicker:false,
			format:'H:i:00',
			step:30
		});

	});
</script>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"><?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Create',
										'buttonType' =>'link',
										'url' => array('fillerData/create')
									),
								),
							)
						);?></div>
					<div class="col-sm-3" style="text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>
					</div>
				</div>
				<div class="box-body">
					<?php //echo $this->renderPartial('_search',array('model'=>$model),true) ?>
					<?PHP
					$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
						'action'=>Yii::app()->createUrl($this->route),
						'method'=>'get',
						'id'=>'form-visible',
					));
					?>
					<div class="col-sm-2 pull-left page-sizes"  >

						<?php echo $form->dropDownListGroup(
							$model,
							'pagination_size',
							array(

								'widgetOptions'=>array(
									'data'=>$model->pages_size(),
									'htmlOptions'=>array(

									),
								),
								'hint'=>''
							)
						); ?>
					</div>
					<?php
					$this->endWidget();
					$this->widget('booster.widgets.TbGridView', array(
						'id'=>'filler-data-form',
						'type' => 'striped bordered condensed',
						'template' => '{pager}{items}{summary}{pager}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'filter' => $model,
						'dataProvider' => $model->search(),
                        'rowCssClassExpression'=>'$data->type == "Image"?"active":($data->type == "Video"?"success":($data->type == "Text"?"info":"danger"))',

						'columns' => array(

						/*	array(
								'name'=>'id',
								'visible'=>$model->visible_id?true:false,
							),*/

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name'=>'text',
								'sortable' => true,
								'visible'=>$model->visible_text?true:false,
								'editable' => array(
									'type'=>'textarea',
									'url' => $this->createUrl('fillerData/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
							),
							array(
/*								'class' => 'booster.widgets.TbEditableColumn',*/
								'name'=>'type',
								'visible'=>$model->visible_type?true:false,
								'filter'=>Yii::app()->params['rule_array']['facebook'],
								'value'=>'$data->type',

							/*	'editable' => array(
									'type' => 'select2',
									'url' => $this->createUrl('fillerData/edit'),
									'source' =>Yii::app()->params['rule_array']['facebook'],
									'placement' => 'right',
									'inputclass' => 'span1',
									'value'=>$model->type,

								),*/
							),

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name'=>'platform_id',
								'visible'=>$model->visible_platform_id?true:false,
								'htmlOptions'=>array('width'=>'100px'),
								'filter'=>Platform::model()->get_all_by_array(),
								'value'=>function($data){
									echo CHtml::tag("i", array("class" => "fa fa-".strtolower($data->platform->title), "aria-hidden" => "true","style"=>"font-size:20px;"))." ";
								},
								'editable' => array(
									'type' => 'select2',
									'url' => $this->createUrl('fillerData/edit'),
									'source' =>Platform::model()->get_all_by_array(),
									'placement' => 'right',
									'inputclass' => 'span1',

								),
							),
							array(
								'name'=>'page_index'
							),
							array(
								'name'=>'start_date',
								'visible'=>$model->visible_start_date?true:false,
							),
							array(
								'name'=>'end_date',
								'visible'=>$model->visible_end_date?true:false,
							),
							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name'=>'publish_time',
								'visible'=>$model->visible_publish_time?true:false,
								'editable' => array(
									'type' => 'time',
									'url' => $this->createUrl('fillerData/edit'),

									'placement' => 'right',
									'inputclass' => 'span1',

								),
							),
						/*	array(
								'name'=>'last_generated',
								'visible'=>$model->visible_last_generated?true:false,
							),

							array(
								'name'=>'created_at',
								'visible'=>$model->visible_created_at?true:false,
							),*/

							array(
								'name'=>'media_url',
								'type'=>'html',
								'visible'=>$model->visible_media_url?true:false,
								'header'=>'Media',
								'htmlOptions'=>array('width'=>'150px'),
								'value'=>function($model){
									if($model->type == "Video" or $model->type=="Youtube"){
										echo '<video width="150" controls ><source src="' . $model->media_url . '" type="video/mp4"><source src="' . $model->media_url . '" type="video/ogg"></video>';
									}elseif($model->type=="Image"){
										echo CHtml::image($model->media_url,$model->text,array("class"=>"img-responsive img-tb-grid-view"));
									}elseif($model->type=="Preview"){
										echo CHtml::link($model->link,$model->link,array('target'=>'_blank'));
									}else
										echo "No Media";
								}
							),

							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{view}{update}{delete}',
								'buttons' => array(
									'view' => array(
										'label' => 'View',
										'icon' => 'fa fa-eye',
									),
									'update' => array(
										'label' => 'Update',
										'icon' => 'fa fa-pencil-square-o',
									),
									'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-times',
									),
								)
							),
						)));?>
				</div>
			</div>
			</div>
		</div>
</section>


<!-- Tour -->
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=thirteen'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'firstFiller',
		'title'        => 'Title',
		'next'         => 'second',
		'buttons'      => array(
			array('name'=>'Previous','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$createLink';}"),

			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Title of the filler',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#filler-data-form_c0',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'second',
		'title'        => 'Type',
		'next'         => 'third',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('firstFiller');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Type of the filler',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#filler-data-form_c1',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'third',
		'title'        => 'Platform',
		'next'         => 'fourth',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Chosen platform',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#filler-data-form_c2',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fourth',
		'title'        => 'Media',
		'next'         => 'fifth',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('third');}"
			),
			array(
				'name'   => 'Next',
				'onclick'=> "js:function(){ $('#yw1').addClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fifth'); }"
			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Inserted media(image,video)',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#filler-data-form_c3',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$createTemplate = Yii::app()->createUrl('fillerData/create',array('#' => 'guider=first'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fifth',
		'title'        => 'Options',
		'next'         => 'sex',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fourth');$('#yw1').removeClass('showhideitem') }"
			),
			array(
				'name'   => 'Next',
				'onclick'=> "js:function(){ $('#yw1').removeClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('sex'); }"
			),
			array('name'=>'Create Tour','classString' => 'tourcolor','onclick'=> "js:function(){   document.location = '$createTemplate'; }"),


			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll(); $('#yw1').removeClass('showhideitem') }"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Create new template',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#yw1',
		'position'      => 9,
		'xButton'       => false,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'sex',
		'title'        => 'Option view',
		'next'         => 'seven',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){ $('#yw1').addClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fifth'); }"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'View this item alone',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-eye',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'seven',
		'title'        => 'Option edit',
		'next'         => 'eight',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('sex');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'update this item',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-pencil-square-o',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=fourteen'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'eight',
		'title'        => 'Option remove',

		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('seven');}"
			),

			array('name'=>'Continue ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Delete this item',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-times',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>

<!--endTour-->
