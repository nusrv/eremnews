<?PHP /* @var $data PostQueue */ ?>
<?PHP



if($data->type == 'Image'){


    $this->renderPartial('image',array('data'=>$data));

}elseif($data->type=='Text'){
    $this->renderPartial('text',array('data'=>$data));
}elseif($data->type=='Video'){
    $this->renderPartial('video',array('data'=>$data));

}else{
    $this->renderPartial('preview',array('data'=>$data));

}
?>