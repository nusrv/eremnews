<?php
/* @var $this PostQueueController */
/* @var $model PostQueue */
/* @var $facebook PostQueue */
/* @var $twitter PostQueue */
/* @var $instagram PostQueue */

$this->pageTitle = "";

Yii::app()->clientScript->registerCss('form-group', '
.form-inline .form-group {
    vertical-align: top;
    width: 100%;
}
@media (min-width: 768px)
{.form-inline .form-control {
    display: inline-block;
    width: 100%;
    vertical-align: middle;
}
}
.my-video-dimensions {
    width: 100%;
    height: 264px;
}
.video-js .vjs-big-play-button {

    top: 40%;
    left: 40%;
    }
.panel-heading {
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
    padding-bottom: 27px;
}
    video#my-video {
    width: 100%;
    height: 269px;
}
.editable-input {
    vertical-align: top;
    display: inherit;
    width: auto;
    white-space: normal;
    zoom: 1;
}
.editableform {
    margin-bottom: 0;
    background-color: #fff;
    padding: 10px;
    border-radius: 9px;
}
.editable-buttons {
    display: block;
    margin: 0 auto;
    text-align: center;
    margin-top: 16px;}
    /*.editable-click, a.editable-click, a.editable-click:hover {
    text-decoration: none;
    border-bottom: 0px;
    color: #444;
}*/
');
Yii::app()->clientScript->registerScript('search', "
$('#search-form').change(function(){
if($('#PostQueue_schedule_date').val() != '' ){
    $(this).submit();
    }
});
$('#PostQueue_schedule_date').attr('readonly','readonly');
$('#PostQueue_to').attr('readonly','readonly');
/*$('#yw28').contentChanged(function(){
});*/
");

$class = 'col-sm-4 small_height';
$counter = 1;
if (isset($Facebook)) {
    $counter++;
}
if (isset($Twitter)) {
    $counter++;
}
if (isset($Instagram)) {
    $counter++;
}

if ($counter == 2) {
    $class = 'col-sm-12 medium_height';
}

if ($counter == 3) {
    $class = 'col-sm-6 large_height';
}

?>
<style>
    .loading-imge{
        max-height:18px;
        display: none;
    }
</style>
<script>

    window.App = {};
    $(window).load(function (){
        App.sh =  function (id){
            $('#search-form').submit();
        };

        App.remove_post = function(id){

            var url = $(id).attr('data-url');
            BootstrapDialog.show({
                message: '<h4>Do you want to remove this post from all platforms?</h4>',
                buttons: [{
                    icon: 'fa fa-remove',
                    label: 'Remove from all platforms',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.post(url,{all:1},function(data){
                            location.reload();
                        });
                    }
                }, {
                    icon: 'fa fa-remove',
                    label: 'Remove from this platform only ',
                    cssClass: 'btn-warning',
                    action: function(){
                        $.post(url,{all:0},function(data){
                            location.reload();
                        });
                    }
                }, {
                    label: 'cancel',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });

        };
        App.restore_posted = function(id){
          var url = $(id).attr('data-url');
            BootstrapDialog.show({
                message: '<h4>Do you want to restore this post?</h4>',
                buttons: [{
                    icon: 'fa fa-check',
                    label: 'restore posted',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.post(url,{id:id},function(data){
                            location.reload();
                        });
                    }
                }, {
                    label: 'cancel',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        };
        App.activate_post = function(id,platform_id,parent_id){
            var messages ='';
            var time_id = $('#time-'+id).val();
            var ids = '#activate_post'+id;
            var d = new Date(time_id);
            var x = new Date();
            $.post('<?PHP echo CController::createUrl('/postQueue/getPosts/') ?>',{id:id,platform_id:platform_id,parent_id:parent_id}, function( data ) {

                data = jQuery.parseJSON(data);

                if(x > d){
                    messages = '<h4>Do you want to add this post on all platforms?</h4><h5   class="alert alert-primary">Warning: if you publish this post it will be posted directly check posting time before continuing</h5>';

                }else if(!data['all']){
                    messages = '<h4>Do you want to add this post on all platforms?</h4><h5   class="alert alert-info">Warning(Add to all platforms): One of the related posts will be published directly check posting time before continuing</h5>';

                }
                else {
                    messages = '<h4>Do you want to add this post on all platforms?</h4>';

                }






                var url = $(ids).attr('data-url');

                BootstrapDialog.show({
                    message: messages,
                    buttons: [{
                        icon: 'fa fa-check',
                        label: 'Add to all platforms',
                        cssClass: 'btn-info',
                        action: function(){
                            $.post(url,{all:1},function(data){
                                location.reload();
                            });
                        }
                    }, {
                        icon: 'fa fa-check',
                        label: 'Add to this platform only ',
                        cssClass: 'btn-primary',
                        action: function(){
                            $.post(url,{all:0},function(data){
                                location.reload();
                            });
                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }]
                });
            });
        };


        App.push_post = function(id){
            var url = $(id).attr('data-url');
            BootstrapDialog.show({
                message: '<h4>Do you want to Push this post to all platforms?</h4>',
                buttons: [{
                    icon: 'fa fa-remove',
                    label: 'Push to all platforms',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.post(url,{all:1},function(data){
                            location.reload();
                        });
                    }
                }, {
                    icon: 'fa fa-remove',
                    label: 'Push to this platform only ',
                    cssClass: 'btn-warning',
                    action: function(){
                        $.post(url,{all:0},function(data){
                            location.reload();
                        });
                    }
                }, {
                    label: 'cancel',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        };


        App.re_post = function(id){
            var url = $(id).attr('data-url');
            BootstrapDialog.show(
            {
                message: '<h4>Do you want to repush this post ?</h4>',
                buttons: [     {
                    icon: 'fa fa-remove',
                    label: 'Repush to all platforms',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.post(url,{all:1},function(data){
                            location.reload();
                        });
                    }
                },{
                    icon: 'fa fa-remove',
                    label: 'Repush to this platform only',
                    cssClass: 'btn-warning',
                    action: function(){
                        $.post(url,{all:0},function(data){
                            location.reload();
                        });
                    }
                }, {
                    label: 'cancel',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        };
        App.edit_pined = function(id,pinned){


            $.post('<?PHP echo CController::createUrl('/postQueue/edit_pined/') ?>',{id:id}, function( data ) {

                if(data==1) {
                    $('#img_pinned_'+id).attr('src','<?php echo Yii::app()->baseUrl.'/image/note2.png'; ?>');
                    $('#img_pinned_'+id).removeAttr('title');
                    $('#img_pinned_'+id).removeAttr('data-original-title');
                    $('#img_pinned_'+id).attr('data-original-title','Unpin');
                }else{
                    $('#img_pinned_'+id).attr('src','<?php echo Yii::app()->baseUrl.'/image/pin_black.png'; ?>');
                    $('#img_pinned_'+id).removeProp('title');
                    $('#img_pinned_'+id).removeAttr('data-original-title');
                    $('#img_pinned_'+id).attr('data-original-title','Pin');

                }



            });


        };
        $('#thematic_post_rendered').change(function(){

            if($(this).val().trim().length !== 0) {
                $('.loading-imge').css('display', 'inline');
                $('#thematic-form').submit();
            }
        });
        $('#thematic_post_rendered').on('paste',function(){
            setTimeout(function () {

                $('.loading-imge').css('display', 'inline');
                $('#thematic-form').submit();

            }, 100);
        })


    });

</script>

<section class="content">
    <div class="row">

        <?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
            'id'=>'thematic-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>true,
            'type' => 'horizontal',
            'action'=>CController::createUrl('/thematic_post/'),
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
        ?>
        <div class="col-sm-12" >

            <?php
            echo $form->textFieldGroup($model,'link',array(
                'widgetOptions'=>array(
                    'htmlOptions'=>array(
                        'name'=>'thematic_post_rendered',
                        'placeholder'=>'Insert a link here to create thematic post',
                        'style'=>'padding:10px;',
                        'autofocus'=>'autofocus'
                    ),
                ),
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5 col-sm-pull-1',
                    'style'=>'margin-left:20px;'
                ),
                'labelOptions'=>array(
                    'label'=>'',
                    'class' => 'col-sm-1 ',

                ),
                'append' => '<a href="#" class="input-sm"  onclick="guiders.show(\'thematic\');return false"    ><i class="fa fa-question-circle" style="font-size: 20px;"></i></a><img src="'.Yii::app()->baseUrl .'/image/updateimg.gif" class="loading-imge" />'



            ));
            ?>



            <?php $this->endWidget(); ?>



            <div class="col-sm-8 col-sm-push-1">
                <div class="col-sm-2"><h3>Quick create:</h3></div>
                <div class="col-sm-8"><h3> <?php echo CHtml::link('Non thematic',CController::CreateUrl('postQueue/create'),array('class'=>'btn btn-info '))?>
                        <?php echo CHtml::link('Thematic',CController::CreateUrl('/thematic_post'),array('class'=>'btn btn-info')) ?>
                    </h3></div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="box">
                <div class="box-header with-border">

                    <?php $this->renderPartial('_adv_search', array('model' => $model,'this',$this)); ?>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="box">
                <div class="box-body post_queue_main">

                    <?php
                    if(Yii::app()->controller->action->id == 'main'){
                        $emptymsg = '<div class="col-sm-12 font-open-sans">There are no scheduled posts to be shown</div>';
                    }elseif(Yii::app()->controller->action->id == 'mainUn'){
                        $emptymsg = '<div class="col-sm-12 font-open-sans">There are no unscheduled posts to be shown</div>';

                    }elseif(Yii::app()->controller->action->id == 'mainPosted'){
                        $emptymsg = '<div class="col-sm-12 font-open-sans">There is no posts that were posted</div>';

                    }elseif(Yii::app()->controller->action->id == 'mainPinned'){
                        $emptymsg = '<div class="col-sm-12 font-open-sans">There is no pinned posts</div>';

                    }
                    ?>

                    <?PHP if (isset($Facebook)) { ?>



                        <div class="<?PHP echo $class ?>">
                            <ul class="timeline">
                                <li class="time-label">
                  <span class="bg-blue">
                    Facebook
                  </span>
                                    <?PHP
                                    $this->widget(
                                        'booster.widgets.TbThumbnails',
                                        array(
                                            'dataProvider' => $Facebook,
                                            'template' => "{items}\n{pager}",
                                            'itemView' => '_view_post',
                                            'emptyText' => $emptymsg,
                                            'ajaxUpdate'=>true,
                                            'afterAjaxUpdate'=>'js:console.log("test")',
                                            'updateSelector'=>'custom-page-selector', //update selector


                                            'pager' => array(
                                                'class' => 'booster.widgets.TbPager',
                                                'maxButtonCount' => 4,
                                                'nextPageLabel' => 'Next',
                                                'prevPageLabel' => 'Previous',
                                                'firstPageLabel' => 'First',
                                                'lastPageLabel' => 'Last',
                                                'htmlOptions' => array(
                                                    'class' => 'pull-left'
                                                )
                                            ),
                                        )
                                    );
                                    ?>
                                </li>
                            </ul>

                        </div>
                    <?PHP } ?>
                    <?PHP if (isset($Twitter)) { ?>
                        <div class="<?PHP echo $class ?>">

                            <ul class="timeline">
                                <li class="time-label">
                  <span class="bg-aqua" style="padding-left:15px;padding-right:15px ;">
                     Twitter

                  </span>
                                    <?PHP
                                    $this->widget(
                                        'booster.widgets.TbThumbnails',
                                        array(
                                            'dataProvider' => $Twitter,
                                            'template' => "{items}\n{pager}",
                                            'itemView' => '_view_post',
                                            'emptyText' => $emptymsg,
                                            'ajaxUpdate'=>true,
                                            'afterAjaxUpdate'=>'js:console.log("test")',
                                            'updateSelector'=>'custom-page-selector', //update selector

                                            'pager' => array(
                                                'class' => 'booster.widgets.TbPager',
                                                'maxButtonCount' => 4,
                                                'nextPageLabel' => 'Next',
                                                'prevPageLabel' => 'Previous',
                                                'firstPageLabel' => 'First',
                                                'lastPageLabel' => 'Last',
                                                'htmlOptions' => array(
                                                    'class' => 'pull-left'
                                                )
                                            ),
                                        )
                                    );
                                    ?>
                                </li>
                            </ul>

                        </div>
                    <?PHP } ?>

                    <?PHP if (isset($Instagram)) { ?>
                        <div class="<?PHP echo $class ?>">
                            <ul class="timeline">
                                <li class="time-label">
                  <span class="bg-purple">
                    Instagram
                  </span>
                                    <?PHP
                                    $this->widget(
                                        'booster.widgets.TbThumbnails',
                                        array(
                                            'dataProvider' => $Instagram,
                                            'template' => "{items}\n{pager}",
                                            'itemView' => '_view_post',
                                            'emptyText' => $emptymsg,
                                            'ajaxUpdate'=>true,
                                            'afterAjaxUpdate'=>'js:console.log("test")',

                                            'updateSelector'=>'custom-page-selector', //update selector

                                            'pager' => array(
                                                'class' => 'booster.widgets.TbPager',
                                                'maxButtonCount' => 4,
                                                'nextPageLabel' => 'Next',
                                                'prevPageLabel' => 'Previous',
                                                'firstPageLabel' => 'First',
                                                'lastPageLabel' => 'Last',
                                                'htmlOptions' => array(
                                                    'class' => 'pull-left'
                                                )
                                            ),
                                        )
                                    );
                                    ?>
                                </li>
                            </ul>
                        </div>
                    <?PHP } ?>
                </div>

            </div>

        </div>
</section>
<?php
$this->widget('ext.eguiders.EGuider', array(
        'id'           => 'thematic',
        'title'        => 'Thematic post',

        // why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
        'description'   => 'Insert a link here to create a post with a predefined post structure template.',
        'overlay'       => true,
        // you can attach your guide to any element in the page thanks to JQuery selectors
        /*        'attachTo'      => '#thematic_post_rendered',*/
        'position'      => 7,
        'xButton'       => true,
        'onShow'        => 'js:function(){ $(".highlight pre").show();}',
        'closeOnEscape' => true,
    )
);
?>
<?php //'confirm'=>'Do you want to delete all posts about this program from this calendar?'
//Yii::app()->createUrl("site/delete/".$data->id) ?>
