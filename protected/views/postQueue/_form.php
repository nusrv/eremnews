<?php
/* @var $this PostQueueController */
/* @var $model PostQueue */
/* @var $form TbActiveForm */
?>
<style type="text/css">
	.form-horizontal .control-label {
		text-align: left;
	}
	.guider_button{
		font-size:12px;    }

</style>

<div class="form" style="padding-top: 30px">

	<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'post-queue-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>true,
		'type' => 'horizontal',
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	));
	$field = 'col-sm-10';
	$label = 'col-sm-2';
	?>



	<input type="hidden" value="<?php echo $model->type; ?>" id="hidden-type" name="hidden-type"/>
	<?php if($model->id != NULL){
		?>
		<div class="col-sm-12">
			<h5 style="color:green;opacity: .5">Step 1 : Highlight the text that you wish to add to this post from the below description[it will be automatically copied]</h5>
			<h5 style="color:green;opacity: .5">Step 2 : Highlight the text that you wish to replace with the new selection [it will be automatically replaced]</h5>
		</div>
	<?php }
	?>
	<div class="col-sm-8" >
		<div class="col-sm-1 col-sm-push-1">
			<a class="btn btn-default input-sm" style="background-color:silver; " id="EnglishDirection"><i class="fa fa-align-left "></i></a>
		</div>
		<div class="col-sm-1 col-sm-push-1">
			<a class="btn btn-default input-sm" style="background-color:silver;" id="ArabicDirections"><i class="fa fa-align-right "></i></a>
		</div>
        <?php
        if($model->isNewRecord) {
            ?>
            <div class="col-sm-1 col-sm-push-1">
                <label class="btn btn-danger"><input type="checkbox" id="checked_hashtags" style="display: none;"/>Hashtag highlighted text</label>
            </div>
            <?php
        }
        ?>
	</div>
	<div class="col-sm-5 ">
		<a href="#"   onclick="guiders.show('createpostfirst');return false"    ><i class="fa fa-question-circle"></i></a>

		<?php echo $form->textAreaGroup($model,'post',array(
			'labelOptions' => array(
				'class' => 'col-sm-2',

			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10 arabic-direction',
				'id'=>'PostDir'
			),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
					'maxlength' => 700, 'rows' => 12, 'cols' => 55
				),
			),
		)); ?>
		<div class="col-sm-12 col-sm-push-2" id="twitter" style="display:none;">
			Number of Characters:
			<small id="twitter_counter" style="color:red;font-size: 18px">140</small>

		</div>
	</div>


	<div class="col-sm-7">
		<a href="#"   onclick="guiders.show('second');return false"    ><i class="fa fa-question-circle"></i></a>

		<?php
		if($model->id != null){
			echo $form->dropDownListGroup($model,'platform_id',array(
				'widgetOptions'=>array(
					'data'=>CHtml::listData(Platform::model()->findAll('deleted=0'),'id','title'),
					'empty'=>'Choose One',
					'htmlOptions'=>array(
						'onchange'=>'javascript:App.change()',
					),
				),
				'wrapperHtmlOptions' => array(
					'class' => $field,
				),

				'labelOptions' => array(
					'class' => $label,
				),
			));
		}

		else{
			echo $form->checkboxListGroup($model, 'platform_id', array(
				'widgetOptions' => array(
					'data' => CHtml::listData(Platform::model()->findAll('deleted=0'), 'id', 'title'),
					'htmlOptions' => array(
						/*					'onchange'=>'javascript:App.change()',*/
						'class' => 'Platforms'
					),
				),
				'wrapperHtmlOptions' => array(
					'class' => $field,

				),


				'labelOptions' => array(
					'class' => $label,
				),
			));
		} ?>
		<a href="#"   onclick="guiders.show('third');return false"    ><i class="fa fa-question-circle"></i></a>

		<?php

		if($model->isNewRecord){
			$array = Yii::app()->params['rule_array']['facebook'];
		}else{
			$array = Yii::app()->params['rule_array'][strtolower($model->platforms->title)];
		}
		echo $form->dropDownListGroup($model,'type',array('rows'=>6, 'cols'=>50,
			'widgetOptions'=>array(
				'data'=>$array,
				'empty'=>'Choose One'
			),
			'groupOptions' => array(
				'id'=>'type_PostQueue'
			),
			'labelOptions' => array(
				'class' => $label,
			),
			'wrapperHtmlOptions' => array(
				'class' => $field,
			),
		)); ?>
		<a href="#"   onclick="guiders.show('fourth');return false"    ><i class="fa fa-question-circle"></i></a>

		<div class="form-group">
			<div class="col-sm-2"><?php echo $form->labelEx($model,'schedule_date',array('label' => Yii::t( 'myApp', 'Scheduled date' ))); ?></div>
			<div class="col-sm-10">
				<?php echo $form->textField($model,'schedule_date',
					array(
						'type'=>'text','id'=>'datetimepicker_format_value','class'=>'form-control'
					)
				);
				?>

				<script>
					$( document ).ready(function() {
						$('#datetimepicker_format_value').datetimepicker({
							format:'Y-m-d H:i',step:5,

							minDate: new Date('Y-m-d H:i'),

						});
					});

				</script>
				<?php echo $form->error($model,'schedule_date'); ?>
			</div>


		</div>


		<?php echo $form->fileFieldGroup($model,'media_url',array('size'=>60,'maxlength'=>255,
			'labelOptions' => array(
				'class' => 'col-sm-2',
			'label' => Yii::t( 'myApp', 'Upload' ),
				'id'=>'change_label'
			),
			'groupOptions' => array(
				'id'=>'file_PostQueue',
				'style'=>'display:none'
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10',
			),
		)); ?>
		<?php echo $form->textAreaGroup($model,'youtube',array(
			'labelOptions' => array(
				'class' => 'col-sm-2',
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10',
			),
			'groupOptions' => array(
				'id'=>'youtube_PostQueue',
				'style'=>'display:none'
			),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
					'maxlength' => 200, 'rows' => 2, 'cols' => 55
				),
			),
		)); ?>
		<a href="#"   onclick="guiders.show('fifth');return false"    ><i class="fa fa-question-circle"></i></a>

		<?php echo $form->dropDownListGroup($model,'catgory_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(Category::model()->findAll('deleted=0 and active=1'),'id','title','page_index'),
				'empty'=>'Choose One',
				/* 'htmlOptions'=>array(
                     'onchange'=>'javascript:App.change()',
                 ),*/
			),
			'wrapperHtmlOptions' => array(
				'class' => $field,
			),

			'labelOptions' => array(
				'class' => $label,
			),
		)); ?>
		<?php echo $form->urlFieldGroup($model,'link',array(
			'labelOptions' => array(
				'class' => 'col-sm-2',
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-10',
			),
			'groupOptions' => array(
				'id'=>'link_PostQueue',
				'style'=>'display:none'
			),
			'widgetOptions'=>array(
				'htmlOptions'=>array(
					'maxlength' => 200, 'rows' => 2, 'cols' => 55
				),
			),
		)); ?>
		<div id="all_call_to_action">

			<input type="checkbox" id="calltoaction" />Enable call to action

			<?php echo $form->dropDownListGroup($model,'type_call_to_action',array(
				'widgetOptions'=>array(
					'data'=>array('SHOP_NOW'=>'SHOP_NOW','BOOK_TRAVEL'=>'BOOK_TRAVEL','LEARN_MORE'=>'LEARN_MORE','SIGN_UP'=>'SIGN_UP','DOWNLOAD'=>'DOWNLOAD','WATCH_MORE'=>'WATCH_MORE','NO_BUTTON'=>'NO_BUTTON','INSTALL_MOBILE_APP'=>'INSTALL_MOBILE_APP','USE_MOBILE_APP'=>'USE_MOBILE_APP','INSTALL_APP'=>'INSTALL_APP','USE_APP'=>'USE_APP','PLAY_GAME'=>'PLAY_GAME','WATCH_VIDEO'=>'WATCH_VIDEO','OPEN_LINK'=>'OPEN_LINK','LISTEN_MUSIC'=>'LISTEN_MUSIC','MOBILE_DOWNLOAD'=>'MOBILE_DOWNLOAD','GET_OFFER'=>'GET_OFFER','GET_OFFER_VIEW'=>'GET_OFFER_VIEW','BUY_NOW'=>'BUY_NOW','BUY_TICKETS'=>'BUY_TICKETS','UPDATE_APP'=>'UPDATE_APP','BET_NOW'=>'BET_NOW','GET_DIRECTIONS'=>'GET_DIRECTIONS','ADD_TO_CART'=>'ADD_TO_CART','ORDER_NOW'=>'ORDER_NOW','SELL_NOW'=>'SELL_NOW'),
					'empty'=>'Choose One',


				),
				'wrapperHtmlOptions' => array(
					'class' => $field,
					/*				'disabled'=>true,*/

				),
				'groupOptions' => array(
					'id'=>'type_call_to_action',

				),

				'labelOptions' => array(
					'class' => $label,

				),
			)); ?>
			<?php
			echo $form->textFieldGroup($model,'call_to_action',array(
				'wrapperHtmlOptions' => array(
					'class' => $field,
					/*				'disabled'=>true,*/

				),
				'labelOptions' => array(
					'class' => $label,

				),
			))
			?>
		</div>

	</div>
	<div class="col-sm-6 ">
		<?php
		News::model()->get_related_text_update($model);
		?>
	</div>
	<?php
	if($model->type == 'Image' or $model->type == 'Preview'){

		echo '<div class="col-sm-6 arabic-direction">';
		MediaNews::model()->get_related_images_update($model);
		echo '</div>';
	}
	?>

	<div class="form-actions   pull-right" style="margin-top: 20px;margin-right:14px;">
		<div class="error-twitter" style="display: none;">
			<div class="alert alert-warning">
				<h4>Please check twitter characters count</h4>
			</div>
		</div>
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'htmlOptions'=>array(
				'name'=>'review',
					'class'=>'sub_btns',
				),

				'context' => 'primary',
				'label' => $model->isNewRecord ? 'Save and review related posts' : 'Save and review related posts'
			)
		); ?>

		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'htmlOptions'=>array(
					'name'=>'postqueue',
					'class'=>'sub_btns',
				),
				'context' => 'primary',
				'label' => $model->isNewRecord ? 'Save and go to post queue' : 'Save and go to post queue',
			)
		); ?>

		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'danger',

				'label' => $model->isNewRecord ? 'Post Now' : 'Post Now',
				'htmlOptions' => array(
					'confirm'=>'Are you sure you want to save?',
					'class'=>'sub_btns',
					'name'=>'postnow'
				)

			)
		); ?>





	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->

<!--Tour-->
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=nine'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'createpostfirst',
		'title'        => 'Post',
		'next'         => 'second',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Post description',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#PostQueue_post',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'second',
		'title'        => 'Platform',
		'next'         => 'third',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The platform which this post will be posted to',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#PostQueue_platform_id',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'third',
		'title'        => 'Type',
		'next'         => 'fourth',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'Your post type(image,video,text,upload from youtube or Preview)',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#PostQueue_type',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fourth',
		'title'        => 'Schedule Date',
		'next'         => 'fifth',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'Time for the post to be posted',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#datetimepicker_format_value',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fifth',
		'title'        => 'Section',
		'next'         => 'six',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'This section will be assigned to your post',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#PostQueue_catgory_id',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'six',
		'title'        => 'Category',
		'next'         => 'seven',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'Click to create the post',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#yw1',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=ten'));
$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'seven',
		'title'        => 'Category',

		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('six');}"
			),
			array('name'=>'Continue ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),



			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'You will find your created post in here',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#PostQueueController',
		'position'      => 3,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<!--EndTour-->