<?php
/* @var $this PdfController */
/* @var $model Pdf */
$this->pageTitle = "PDF | Create";

$this->breadcrumbs = array('PDF File' => array('index'), 'Download',);
?>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="col-sm-9"> <?php
                        $this->widget('booster.widgets.TbButtonGroup',
                            array(
                                'size' => 'small',
                                'context' => 'info',
                                'buttons' => array(
                                    array(
                                        'label' => 'Manage',
                                        'buttonType' =>'link',
                                        'url' => array('pdf/admin')
                                    ),
                                ),

                            )
                        );
                        ?></div>
                    <div class="col-sm-3" style="text-align: left;">
                       <?php echo Yii::app()->params['statement']['previousPage']; ?>




                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <?PHP
                            foreach(Yii::app()->user->getFlashes() as $key => $message) {
                                echo '<div class="alert alert-' . $key . '">' . $message . "</div>";
                            }
                            ?>
                        </div>
                    </div>
                    <?php $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
</section>
