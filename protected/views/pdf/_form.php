<?php
/* @var $this PdfController */
/* @var $model Pdf */
/* @var $form TbActiveForm */
?>
	<style>
		.showhideitem{
			display: block;
		}
	</style>
<div class="col-sm-12">
	<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'pdf-form',
		'enableAjaxValidation'=>true,
		'type' => 'horizontal',
	));
	$field = 'col-sm-11';
	$label = 'col-sm-1';
	echo $form->errorSummary($model);
	?>
	<div class="form-group" style="margin-bottom: 20px;">
		<div class="col-md-3"><?php echo $form->labelEx($model,'from_date'); ?></div>
		<div class="col-md-9">
			<?php
			$time = strtotime(date('Y-m-d'));

			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model'=>$model,
				'attribute'=>'from_date',
				'name'=>'from_date',

				//'value'=>isset($_POST["from"]) ? $_POST["from"] : '',
				'options'=>array(

					'changeMonth'=>true,
					'changeYear'=>true,
					'dateFormat' => 'yy-mm-dd',

					'maxDate'=> date('Y-m-d',strtotime("+1 month", $time)),
				),
				'htmlOptions'=>array(
					'class'=>'form-control ',
					'readonly'=>'readonly',
				),
			));
			?>
			<?php echo $form->error($model,'from_date',array(
				'style'=>'color:red;'
			)); ?>
		</div>
	</div>

	<div class="form-group" style="    margin-bottom: 20px;">
		<div class="col-md-3"><?php echo $form->labelEx($model,'to_date'); ?></div>
		<div class="col-md-9">
			<?php

			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model'=>$model,
				'attribute'=>'to_date',
				'name'=>'to_date',
				//'value'=>isset($_POST["from"]) ? $_POST["from"] : '',
				// additional javascript options for the date picker plugin
				'options'=>array(
					'changeMonth'=>true,
					'changeYear'=>true,
					'dateFormat' => 'yy-mm-dd',
					'minDate'=> date('Y-m-d',strtotime("+1 day", $time)),
					'maxDate'=> date('Y-m-d',strtotime("+1 month", $time)),
				),
				'htmlOptions'=>array(
					'class'=>'form-control ',
					'readonly'=>'readonly',
				),
			));
			?>
			<?php echo $form->error($model,'to_date',array(
				'style'=>'color:red'
			)); ?>
		</div>
	</div>
	<div class="form-actions col-md-12  pull-right" style="margin-bottom: 20px;margin-right: -15px;">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => 'Download',
				'htmlOptions'=>array(
					'class'=>'pull-right'
				)
			)
		); ?>

	</div>
	<?php $this->endWidget(); ?>
</div><!-- form -->

<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=seventeen'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'firstPdf',
		'title'        => 'From Date',
		'next'         => 'second',
		'buttons'      => array(
			array('name'=>'Previous','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$createLink';}"),

			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'input start date of the pdf you wanted',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Pdf_from_date',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'second',
		'title'        => 'To Date',
		'next'         => 'third',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('firstPdf');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'input end date of the pdf you wanted',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Pdf_to_date',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>

<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'third',
		'title'        => 'Create PDF',
		'next'         => 'fourth',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}"
			),
			array(
				'name'   => 'Next',
				'onclick'=> "js:function(){ $('#yw1').addClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('createPdf'); }"
			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Press and your PDF will be downloaded',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#yw4',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$createTemplate = Yii::app()->createUrl('pdf/admin',array('#' => 'guider=firstpdf'));
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=seventeen'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'createPdf',
		'title'        => 'Action Manage',
		'next'         => 'third',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('third');$('#yw1').removeClass('showhideitem') }"
			),
			array('name'=>'Continue ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),

			array('name'=>'Manage Tour','classString' => 'tourcolor','onclick'=> "js:function(){   document.location = '$createTemplate'; }"),


			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll(); $('#yw1').removeClass('showhideitem') }"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Here you will find all downloaded Pdfs ',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#yw1',
		'position'      => 9,
		'xButton'       => false,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$Previous = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=postQueueTour'));
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=FinalStepTour'));
$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'PDFTour',
		'title'        => 'PDF file',
		'next' =>'EditPostsTour',
		'buttons'      => array(
			array('name'=>'Previous ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$Previous';}"),

			array('name'=>'Next ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),



			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'You can also export the created content into a PDF file, should you wish to share the content with your colleagues, supervisors or clients. ',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#PDFtour',
		'position'      => 3,
		'xButton'       => true,
		'autoFocus'      => true,
		'closeOnEscape' => true,
	)
);
?>
