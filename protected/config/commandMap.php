<?php
return  array(

    'FeatureGenerator' => array(
        'class' => 'application.modules.features.commands.FeatureGeneratorCommand',
    ),
    'TwitterFeedGenerator' => array(
        'class' => 'application.modules.split_report.commands.TwitterFeedCommand',
    ),
    'FacebookFeedGenerator' => array(
        'class' => 'application.modules.split_report.commands.FacebookFeedCommand',
    ),
    'InstagramFeedGenerator' => array(
        'class' => 'application.modules.split_report.commands.InstagramFeedCommand',
    ),


);