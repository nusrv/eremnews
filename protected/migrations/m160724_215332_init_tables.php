<?php

class m160724_215332_init_tables extends CDbMigration
{
	public function up()
	{

		$this->execute("
		-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 25, 2016 at 12:51 AM
-- Server version: 5.5.50-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
SET time_zone = \"+00:00\";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thenational`
--

-- --------------------------------------------------------

--
-- Table structure for table `archives_media_news`
--

CREATE TABLE IF NOT EXISTS `archives_media_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `type` enum('image','text','video') NOT NULL,
  `is_archives` tinyint(1) DEFAULT '1',
  `media` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `news_id` (`news_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `archives_news`
--

CREATE TABLE IF NOT EXISTS `archives_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(1000) NOT NULL,
  `link_md5` varchar(225) NOT NULL,
  `description` varchar(255) NOT NULL,
  `shorten_url` varchar(255) NOT NULL,
  `publishing_date` datetime NOT NULL,
  `is_archives` tinyint(1) NOT NULL DEFAULT '1',
  `schedule_date` datetime NOT NULL,
  `generated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `link_md5` (`link_md5`(191)),
  KEY `category_id` (`category_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `archives_post_queue`
--

CREATE TABLE IF NOT EXISTS `archives_post_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post` text NOT NULL,
  `type` enum('text','image','video','pending') NOT NULL,
  `schedule_date` datetime NOT NULL,
  `catgory_id` int(11) NOT NULL,
  `is_posted` tinyint(1) NOT NULL DEFAULT '0',
  `post_id` varchar(255) DEFAULT NULL,
  `is_scheduled` tinyint(1) NOT NULL DEFAULT '0',
  `platform_id` int(11) NOT NULL,
  `generated` enum('auto','manual') NOT NULL DEFAULT 'auto',
  `parent_id` int(11) DEFAULT NULL,
  `is_archives` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cover_photo`
--

CREATE TABLE IF NOT EXISTS `cover_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `media_url` varchar(255) NOT NULL,
  `schedule_date` varchar(255) NOT NULL,
  `is_posted` tinyint(1) NOT NULL DEFAULT '0',
  `platform_id` int(11) NOT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `platform_id` (`platform_id`),
  KEY `platform_id_2` (`platform_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `filler_data`
--

CREATE TABLE IF NOT EXISTS `filler_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `media_url` varchar(255) DEFAULT NULL,
  `type` enum('Text','Image','Preview','Video','Carousel','Albums','Multiple_Image') NOT NULL,
  `platform_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `publish_time` time NOT NULL,
  `last_generated` date DEFAULT NULL,
  `catch` tinyint(1) DEFAULT '0',
  `call_to_action` varchar(255) DEFAULT NULL,
  `type_call_to_action` enum('SHOP_NOW','BOOK_TRAVEL','LEARN_MORE','SIGN_UP','DOWNLOAD','WATCH_MORE','NO_BUTTON','INSTALL_MOBILE_APP','USE_MOBILE_APP','INSTALL_APP','USE_APP','PLAY_GAME','WATCH_VIDEO','OPEN_LINK','LISTEN_MUSIC','MOBILE_DOWNLOAD','GET_OFFER','GET_OFFER_VIEW','BUY_NOW','BUY_TICKETS','UPDATE_APP','BET_NOW','GET_DIRECTIONS','ADD_TO_CART','ORDER_NOW','SELL_NOW') DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `platform_id` (`platform_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hashtag`
--

CREATE TABLE IF NOT EXISTS `hashtag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `media_url` varchar(255) DEFAULT NULL,
  `type` enum('Text','Image','Preview','Video','Carousel','Albums','Multiple_Image') NOT NULL,
  `platform_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `publish_time` time NOT NULL,
  `last_generated` date DEFAULT NULL,
  `catch` tinyint(1) DEFAULT '0',
  `call_to_action` varchar(255) DEFAULT NULL,
  `type_call_to_action` enum('SHOP_NOW','BOOK_TRAVEL','LEARN_MORE','SIGN_UP','DOWNLOAD','WATCH_MORE','NO_BUTTON','INSTALL_MOBILE_APP','USE_MOBILE_APP','INSTALL_APP','USE_APP','PLAY_GAME','WATCH_VIDEO','OPEN_LINK','LISTEN_MUSIC','MOBILE_DOWNLOAD','GET_OFFER','GET_OFFER_VIEW','BUY_NOW','BUY_TICKETS','UPDATE_APP','BET_NOW','GET_DIRECTIONS','ADD_TO_CART','ORDER_NOW','SELL_NOW') DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `platform_id` (`platform_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `row_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_host` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `media_news`
--

CREATE TABLE IF NOT EXISTS `media_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `type` enum('image','text','video','gallery') NOT NULL,
  `media` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `news_id` (`news_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  `column` varchar(255) NOT NULL,
  `num_read` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `link_md5` varchar(225) NOT NULL,
  `description` text,
  `shorten_url` varchar(255) NOT NULL,
  `publishing_date` datetime NOT NULL,
  `schedule_date` datetime NOT NULL,
  `generated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `link_md5` (`link_md5`(191)),
  KEY `category_id` (`category_id`),
  KEY `sub_category_id` (`sub_category_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pdf`
--

CREATE TABLE IF NOT EXISTS `pdf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `generated` tinyint(1) NOT NULL DEFAULT '0',
  `media_url` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `platform`
--

CREATE TABLE IF NOT EXISTS `platform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `platform`
--

INSERT INTO `platform` (`id`, `title`, `deleted`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Facebook', 0, '2016-04-19 00:00:00', NULL, NULL, NULL),
(2, 'Twitter', 0, '2016-04-19 00:00:00', NULL, NULL, NULL),
(3, 'Instagram', 0, '2016-04-19 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_queue`
--

CREATE TABLE IF NOT EXISTS `post_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post` text NOT NULL,
  `type` enum('Text','Image','Preview','Video','Carousel','Albums','Multiple_Image') NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `media_url` varchar(255) DEFAULT NULL,
  `settings` enum('general','custom') NOT NULL DEFAULT 'general',
  `schedule_date` datetime NOT NULL,
  `catgory_id` int(11) DEFAULT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `is_posted` tinyint(1) NOT NULL DEFAULT '0',
  `post_id` varchar(255) DEFAULT NULL,
  `news_id` int(11) DEFAULT NULL,
  `is_scheduled` tinyint(1) NOT NULL DEFAULT '0',
  `platform_id` int(11) NOT NULL,
  `generated` enum('auto','manual','clone') NOT NULL DEFAULT 'auto',
  `errors` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `call_to_action` varchar(255) DEFAULT NULL,
  `type_call_to_action` enum('SHOP_NOW','BOOK_TRAVEL','LEARN_MORE','SIGN_UP','DOWNLOAD','WATCH_MORE','NO_BUTTON','INSTALL_MOBILE_APP','USE_MOBILE_APP','INSTALL_APP','USE_APP','PLAY_GAME','WATCH_VIDEO','OPEN_LINK','LISTEN_MUSIC','MOBILE_DOWNLOAD','GET_OFFER','GET_OFFER_VIEW','BUY_NOW','BUY_TICKETS','UPDATE_APP','BET_NOW','GET_DIRECTIONS','ADD_TO_CART','ORDER_NOW','SELL_NOW') DEFAULT NULL,
  `pinned` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catgory_id` (`catgory_id`),
  KEY `catgory_id_2` (`catgory_id`),
  KEY `parent_id` (`parent_id`),
  KEY `platform_id` (`platform_id`),
  KEY `news_id` (`news_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post_template`
--

CREATE TABLE IF NOT EXISTS `post_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('Text','Image','Preview','Video','Carousel','Albums','Multiple_Image') NOT NULL DEFAULT 'Text',
  `catgory_id` int(11) DEFAULT NULL,
  `text` text NOT NULL,
  `platform_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `platform_id` (`platform_id`),
  KEY `catgory_id` (`catgory_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `post_template`
--

INSERT INTO `post_template` (`id`, `type`, `catgory_id`, `text`, `platform_id`, `deleted`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(19, 'Preview', NULL, '[title] \r\n[author] \r\n#الإمارات_اليوم | [section]', 1, 0, '2016-07-12 17:22:05', NULL, 1, NULL),
(20, 'Preview', NULL, '[title] \r\n[author] \r\n#الإمارات_اليوم | [section] \r\n[short_link]', 2, 0, '2016-07-12 17:22:44', NULL, 1, NULL),
(21, 'Image', NULL, '[title] \r\n\r\n[description] \r\n\r\n[author] \r\n\r\nالمزيد من التفاصيل على #الإمارات_اليوم ', 3, 0, '2016-07-12 17:23:04', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profile_pic`
--

CREATE TABLE IF NOT EXISTS `profile_pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `media_url` varchar(255) NOT NULL,
  `schedule_date` varchar(255) NOT NULL,
  `is_posted` tinyint(1) NOT NULL DEFAULT '0',
  `platform_id` int(11) NOT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `platform_id` (`platform_id`),
  KEY `platform_id_2` (`platform_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `report_facebook`
--

CREATE TABLE IF NOT EXISTS `report_facebook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` int(255) DEFAULT NULL,
  `subscribed` int(255) DEFAULT NULL,
  `likes` int(255) DEFAULT NULL,
  `shares` int(255) DEFAULT NULL,
  `comments` int(255) DEFAULT NULL,
  `impressions_unpaid` int(255) DEFAULT NULL,
  `impressions_paid` int(255) DEFAULT NULL,
  `post_consumptions` int(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `row_id` (`row_id`,`updated_by`,`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `report_instagram`
--

CREATE TABLE IF NOT EXISTS `report_instagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` int(255) DEFAULT NULL,
  `comments` int(255) DEFAULT NULL,
  `likes` int(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `row_id` (`row_id`,`updated_by`,`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `report_twitter`
--

CREATE TABLE IF NOT EXISTS `report_twitter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` int(255) DEFAULT NULL,
  `retweet` int(255) DEFAULT NULL,
  `favorite` int(255) DEFAULT NULL,
  `reply` int(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `row_id` (`row_id`,`updated_by`,`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `how_many_posts` int(11) DEFAULT NULL,
  `start_date_time` time NOT NULL,
  `end_date_time` time NOT NULL,
  `gap_time` int(11) NOT NULL DEFAULT '10',
  `updated_at` datetime DEFAULT NULL,
  `jobs_posts_per_day` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `jobs_posts_per_week` int(11) DEFAULT NULL,
  `date_check_job` date DEFAULT NULL,
  `timezone` varchar(255) NOT NULL,
  `direct_push` tinyint(1) NOT NULL DEFAULT '0',
  `direct_push_start` time NOT NULL,
  `direct_push_end` time NOT NULL,
  `generator_is_running` tinyint(1) DEFAULT NULL,
  `pinned` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `how_many_posts`, `start_date_time`, `end_date_time`, `gap_time`, `updated_at`, `jobs_posts_per_day`, `category_id`, `jobs_posts_per_week`, `date_check_job`, `timezone`, `direct_push`, `direct_push_start`, `direct_push_end`, `generator_is_running`, `pinned`, `created_at`, `created_by`, `updated_by`) VALUES
(1, 15, '00:00:00', '23:30:00', 10, '2016-07-17 20:42:06', 1, NULL, 1, '2016-07-24', 'Africa/Abidjan', 0, '00:00:00', '00:00:00', 0, 0, '2016-07-24 10:22:11', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `gender` enum('m','f') NOT NULL,
  `background_color` varchar(255) NOT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `role` enum('admin','editor') NOT NULL DEFAULT 'editor',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`,`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `email`, `password`, `active`, `gender`, `background_color`, `profile_pic`, `role`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted`) VALUES
(1, 'admin', 'admin', 'ahmad.ghuneim@hotmail.com', '202cb962ac59075b964b07152d234b70', 1, 'm', 'skin-red', '', 'admin', '2016-07-04 23:19:20', '2016-07-24 21:46:05', NULL, 1, 0),
(2, 'ahmad', 'ahmad', 'ahmad@hotmail.com', '202cb962ac59075b964b07152d234b70', 1, 'm', 'skin-purple', NULL, 'admin', '2016-07-05 00:47:25', '2016-07-21 08:39:07', 1, 1, 0);

-- --------------------------------------------------------


--
-- Constraints for dumped tables
--

--
-- Constraints for table `cover_photo`
--
ALTER TABLE `cover_photo`
  ADD CONSTRAINT `cover_photo_ibfk_1` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `filler_data`
--
ALTER TABLE `filler_data`
  ADD CONSTRAINT `platform` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `news_ibfk_2` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `post_queue`
--
ALTER TABLE `post_queue`
  ADD CONSTRAINT `post_queue_ibfk_1` FOREIGN KEY (`catgory_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `post_queue_ibfk_2` FOREIGN KEY (`catgory_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `post_queue_ibfk_3` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `post_queue_ibfk_4` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `post_template`
--
ALTER TABLE `post_template`
  ADD CONSTRAINT `post_template_ibfk_1` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
		");

	}

	public function down()
	{
		echo "m160724_215332_init_tables does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}