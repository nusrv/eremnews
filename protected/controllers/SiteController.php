<?php
class SiteController extends BaseController {

    public function actions()
    {
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            'page'=>array(
                'class'=>'CViewAction',
            ),
            'coco'=>array(
                'class'=>'CocoAction',
            ),
        );
    }

    public function actionIndex(){
        $this->render('index');
    }

    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionChange_show(){

        if(isset(Yii::app()->session['tab_open'])){
            if(Yii::app()->session['tab_open']){
                Yii::app()->session['tab_open'] = false;
            }else{
                Yii::app()->session['tab_open'] = true;
            }
        }else{
            Yii::app()->session['tab_open'] = true;
        }

        echo Yii::app()->session['tab_open'];
    }

    public function actionLogin()
    {
        $model = new LoginForm;
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        $this->render('login', array('model' => $model));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}