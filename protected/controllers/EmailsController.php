<?php

class EmailsController extends BaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','edit','admin','delete'),
				'users'=>array(Yii::app()->user->getState('type')),
			),
		/*	array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array(Yii::app()->user->getState('type')),
			),*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionEdit(){
		if(isset($_POST)){
			if(isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk']) && isset($_POST['scenario'])){
				$model = $this->loadModel($_POST['pk']);

				if(!empty($_POST['name'])){
					$name = $_POST['name'];
					$value = $_POST['value'];
					$model->$name = $value;
					if($model->save())
						echo true;
					else
						echo false;
				}
			}

		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Emails;

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Emails']))
		{


			$model->attributes=$_POST['Emails'];

			$model->created_at = date('Y-m-d H:i:s');
			if($model->save()) {
				Yii::app()->user->setFlash('create', 'Thank you for add conetns ... . .... ');
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Emails']))
		{
			$model->attributes=$_POST['Emails'];
			if($model->save()) {
				Yii::app()->user->setFlash('update', 'Thank you ... . .... ');

				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Emails');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{




		// Uncomment the following line if AJAX validation is needed

		$model_create=new Emails;
		$this->performAjaxValidation($model_create);
		if(isset($_POST['Emails']))
		{


			$model_create->attributes=$_POST['Emails'];

			$model_create->created_at = date('Y-m-d H:i:s');
			if($model_create->save()) {
				$model_create->unsetAttributes();  // clear any default values
				$this->redirect(array('admin'));

				Yii::app()->user->setFlash('create', 'Thank you for add conetns ... . .... ');

			}
		}



		$model=new Emails('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Emails'])) {
			$this->data_search = $_GET['Emails'];
			$this->data_search($model);
			$model->attributes = $_GET['Emails'];
		}
		$this->render('admin',array(
			'model'=>$model,
			'model_create'=>$model_create,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Emails the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Emails::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Emails $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='emails-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
