<?php
//Yii::import('application.commands.BaseCommand');
class FacebookCommand extends BaseCommand{

    private $id = null;

    private $id_fb = null;

    private $post = null;




    public function run($args)
    {
        $this->TimeZone();
        $data = $this->News();

        if(!empty($data)){
            foreach ($data as $value) {


                //catch the post before posting processing...
                if(!empty($value->news_id)){


                    $this->type_post =$value->type;
                    $this->platform_name ='Facebook';

                    $this->check_and_update_news($value->news_id,$value->id,$value->platform_id,$value->template_id);

                    $value=PostQueue::model()->findByPk($value->id);

                }
                $value->is_posted=3;
                $value->save(false);

                $this->id = $value->id;
                $valid = false;
                switch ($value->type) {
                    case 'Image':
                        $this->post = $value;
                        if($this->image()){
                            $valid = true;
                        }
                        break;
                    case 'Video':
                        $this->post = $value;
                        if( $this->video()){
                            $valid = true;
                        }
                        break;
                    case 'Text':
                        $this->post = $value;
                        if($this->text()){
                            $valid = true;
                        }
                        break;
                    case 'Preview':
                        $this->post = $value;
                        if($this->Preview()){
                            $valid = true;
                        }
                        break;
                }

                $db = PostQueue::model()->findByPk($this->post->id);
                if($valid){
                    $db->is_posted = 1;
                    $db->post_id = $this->id_fb;
                    $db->command = false;
                    $db->save();
                }else{
                    $db->is_posted = 2;
                    $db->post_id = $this->id_fb;
                    $db->command = false;
                    $db->save();
                }
                break;
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @return PostQueue the News
     * @var $data PostQueue
     * @var $value PostQueue
     * @var $db PostQueue
     */

    private function News(){

       return PostQueue::model()->model()->get_post_facebook();
    }

    /**


     * @var $post PostQueue
     */

    private function image(){

        $page = 'main';
        if(!empty($this->post->catgory_id))
            if(isset($this->post->catgory->page_index))
                $page = $this->post->catgory->page_index;

        list($facebook,$PAGE_TOKEN)=$this->Load($page);
        if(!empty($this->post->media_url)){
            $image = Yii::app()->params['webroot'].'/image/facebook_'.time().'.jpg';
            if(!@file_put_contents($image,$this->file_get_contents_curl($this->post->media_url))){
                return false;
            }
            sleep(1);
        }

        $data = [
            'message' =>trim($this->post->post),
            'source' =>$facebook->fileToUpload($image),
        ];

        try {
            $response = $facebook->post('/'.Yii::app()->params[$page]['facebook']['page_id'].'/photos', $data, $PAGE_TOKEN);
            $graphNode = $response->getGraphNode();
            if(file_exists($image))
            unlink($image);
            return $this->id_fb =  $graphNode['id'];
        } catch(Facebook\Exceptions\FacebookResponseException $e) {

            echo 'Graph returned an error: ' . $e->getMessage();
            if(file_exists($image))
                unlink($image);
            $this->text();
        } catch(Facebook\Exceptions\FacebookSDKException $e) {


            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            if(file_exists($image))
                unlink($image);
            $this->text();
        }

    }
    function file_get_contents_curl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    private function text(){

        $page = 'main';
        if(!empty($this->post->catgory_id))
            if(isset($this->post->catgory->page_index))
                $page = $this->post->catgory->page_index;

        list($facebook,$PAGE_TOKEN)=$this->Load($page);
        $data = [
            'message' =>$this->post->post,
        ];

        try {
            $response = $facebook->post('/'.Yii::app()->params[$page]['facebook']['page_id'].'/feed', $data, $PAGE_TOKEN);
            $graphNode = $response->getGraphNode();
            return $this->id_fb =  $graphNode['id'];
        } catch(Facebook\Exceptions\FacebookResponseException $e) {


            echo 'Graph returned an error: ' . $e->getMessage();
            return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {

            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            return false;
        }

    }

    private function Preview(){

        $page = 'main';
        if(!empty($this->post->catgory_id))
            if(isset($this->post->catgory->page_index))
                $page = $this->post->catgory->page_index;

        list($facebook,$PAGE_TOKEN)=$this->Load($page);


        $link = $this->post->link;

        if(isset($this->post->news))
          if($this->post->link == $this->post->news->shorten_url)
            $link = $this->post->news->link;

        $data = [
            'message' =>$this->post->post,
            'link' => $link,
        ];

        /*if(!empty($this->post->media_url) && (exif_imagetype($this->post->media_url))){*/
        if(!empty($this->post->media_url)){
            $data['source']=$this->post->media_url;
            if(!empty($this->post->news_id)){


                $news = $this->get_news_row($this->post->news_id);

                $data['caption']='eremnews.com';

               /* $news->title = htmlspecialchars(trim($news->title), ENT_QUOTES, 'UTF-8');*/

                /*$news->description = htmlspecialchars(trim($news->description), ENT_QUOTES, 'UTF-8');
                if(strlen($news->description) >= 3000)
                $data['description'] = trim(substr_replace(trim($news->description),0,3000));
                else
                    $data['description'] = trim($news->description);*/


                $data['description'] = 'إرم نيوز';

                //$data['name'] = trim(mb_substr(trim($news->title),0,100,"UTF-8"));
                $data['name'] = $news->title;
                //$data['caption']=isset($this->post->news->category)?$this->post->news->category->title:'EMARATALYOUM.COM';
            }
        }

        try {
            $response = $facebook->post('/'.Yii::app()->params[$page]['facebook']['page_id'].'/feed', $data, $PAGE_TOKEN);
            $graphNode = $response->getGraphNode();
            return $this->id_fb =  $graphNode['id'];
        } catch(Facebook\Exceptions\FacebookResponseException $e) {

            echo 'Graph returned an error: ' . $e->getMessage();
            return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            return false;
        }

    }

    private function get_news_row($id){

        return News::model()->findByPk($id);
    }

    private function video(){

        $page = 'main';
        if(!empty($this->post->catgory_id))
            if(isset($this->post->catgory->page_index))
                $page = $this->post->catgory->page_index;

        list($facebook,$PAGE_TOKEN)=$this->Load($page);
        $message = $this->post->post;

        if(!empty($this->post->media_url)){
            $ext=explode('.',$this->post->media_url);
            $ext=$ext[count($ext)-1];
            //Store in the filesystem.
            file_put_contents(Yii::app()->params['webroot']."/image/video.".$ext,file_get_contents($this->post->media_url));
            $data = [
                'message' =>$message,
                'description' =>$message,
                'source' =>$facebook->videoToUpload(Yii::app()->params['webroot']."/image/video.".$ext),
            ];

        }else{
            $data = [
                'message' =>$message,
                'description' =>$message,

            ];
        }

        try {

            $response = $facebook->post('/'.Yii::app()->params[$page]['facebook']['page_id'].'/videos', $data, $PAGE_TOKEN);

            $graphNode = $response->getGraphNode();
            return $this->id_fb =  $graphNode['id'];
        } catch(Facebook\Exceptions\FacebookResponseException $e) {


            echo 'Graph returned an error: ' . $e->getMessage();
            $this->text();
        } catch(Facebook\Exceptions\FacebookSDKException $e) {


            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            $this->text();
        }
    }


}