<?php
class PostQueueArchivesCommand extends BaseCommand{

    public function run($args){
        $this->TimeZone();
        $data = $this->GetPostBeforeOneMonth();

        foreach ($data as $item) {

            if($this->SetNewArchives($item)){
               $item->command= false;
               $item->delete();
            }
        }

    }

    public function GetPostBeforeOneMonth(){

        return PostQueue::model()->findAllByAttributes(array('is_posted'=>1));

    }

    public function SetNewArchives($data){
        $model = new ArchivesPostQueue();
        $model->attributes = $data->attributes;
        $model->is_archives =1;
        $model->command= false;
        $model->setIsNewRecord(true);
        $model->id = null;
        return $model->save();

    }

}