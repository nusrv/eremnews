-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost:80
-- Generation Time: Sep 04, 2016 at 05:38 PM
-- Server version: 5.7.13-0ubuntu0.16.04.2
-- PHP Version: 7.0.8-0ubuntu0.16.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eremnews`
--

-- --------------------------------------------------------

--
-- Table structure for table `archives_media_news`
--

CREATE TABLE `archives_media_news` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `type` enum('image','text','video') NOT NULL,
  `is_archives` tinyint(1) DEFAULT '1',
  `media` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `archives_news`
--

CREATE TABLE `archives_news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(1000) NOT NULL,
  `link_md5` varchar(225) NOT NULL,
  `description` varchar(255) NOT NULL,
  `shorten_url` varchar(255) NOT NULL,
  `publishing_date` datetime NOT NULL,
  `is_archives` tinyint(1) NOT NULL DEFAULT '1',
  `schedule_date` datetime NOT NULL,
  `generated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `archives_post_queue`
--

CREATE TABLE `archives_post_queue` (
  `id` int(11) NOT NULL,
  `post` text NOT NULL,
  `type` enum('text','image','video','pending') NOT NULL,
  `schedule_date` datetime NOT NULL,
  `catgory_id` int(11) NOT NULL,
  `is_posted` tinyint(1) NOT NULL DEFAULT '0',
  `post_id` varchar(255) DEFAULT NULL,
  `is_scheduled` tinyint(1) NOT NULL DEFAULT '0',
  `platform_id` int(11) NOT NULL,
  `generated` enum('auto','manual') NOT NULL DEFAULT 'auto',
  `parent_id` int(11) DEFAULT NULL,
  `is_archives` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `page_index` enum('main','other') DEFAULT NULL,
  `lang` enum('ar','en') DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `type` enum('rss','dom') DEFAULT NULL,
  `url_rss` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `page_index`, `lang`, `url`, `deleted`, `active`, `type`, `url_rss`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'أخبار', 'main', 'ar', 'http://www.eremnews.com/category/news', 0, 1, 'dom', NULL, '2016-08-31 14:02:20', '2016-09-04 13:36:03', 1, 1),
(2, 'اقتصاد', 'main', 'ar', 'http://www.eremnews.com/category/economy', 0, 1, 'dom', NULL, '2016-08-31 14:03:08', '2016-09-04 13:36:04', 1, 1),
(3, 'منوعات', 'main', 'ar', 'http://www.eremnews.com/category/entertainment', 0, 1, 'dom', NULL, '2016-08-31 14:03:37', '2016-09-04 13:36:07', 1, 1),
(4, 'علوم وتقنية', 'main', 'ar', 'http://www.eremnews.com/category/sciences-technology', 0, 1, 'dom', NULL, '2016-08-31 14:03:56', '2016-09-04 13:36:06', 1, 1),
(5, 'السعودية', 'main', 'ar', 'http://www.eremnews.com/category/news/arab-word/saudi-arabia', 0, 1, 'dom', NULL, '2016-08-31 14:05:18', '2016-09-04 13:36:05', 1, 1),
(6, 'الخليج', 'main', 'ar', 'http://www.eremnews.com/category/news/arab-word/gcc', 0, 1, 'dom', NULL, '2016-08-31 14:05:46', '2016-09-04 13:36:05', 1, 1),
(7, 'اليمن', 'main', 'ar', 'http://www.eremnews.com/category/news/arab-word/yemen', 0, 1, 'dom', NULL, '2016-08-31 14:06:16', '2016-09-04 13:36:06', 1, 1),
(8, 'رياضة', 'other', 'ar', 'http://www.eremnews.com/category/sports', 0, 1, 'dom', NULL, '2016-08-31 14:06:38', '2016-09-04 13:11:35', 1, 1),
(9, 'selections', NULL, NULL, 'http://www.eremnews.com//selections', 0, 1, NULL, NULL, '2016-09-01 11:09:03', '2016-09-04 13:36:02', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_level`
--

CREATE TABLE `category_level` (
  `id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `predication` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `category_level`
--

INSERT INTO `category_level` (`id`, `source_id`, `predication`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 1, 'figure[class=entry-image-container] a', '2016-08-31 14:09:19', NULL, 1, NULL),
(2, 2, 'figure[class=entry-image-container] a', '2016-08-31 14:09:19', NULL, 1, NULL),
(3, 3, 'figure[class=entry-image-container] a', '2016-08-31 14:09:19', NULL, 1, NULL),
(4, 4, 'figure[class=entry-image-container] a', '2016-08-31 14:09:20', NULL, 1, NULL),
(5, 5, 'figure[class=entry-image-container] a', '2016-08-31 14:09:20', NULL, 1, NULL),
(6, 6, 'figure[class=entry-image-container] a', '2016-08-31 14:09:20', NULL, 1, NULL),
(7, 7, 'figure[class=entry-image-container] a', '2016-08-31 14:09:20', NULL, 1, NULL),
(8, 8, 'figure[class=entry-image-container] a', '2016-08-31 14:09:20', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_source`
--

CREATE TABLE `category_source` (
  `id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` enum('rss','dom') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cover_photo`
--

CREATE TABLE `cover_photo` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `page_index` enum('main','other') DEFAULT NULL,
  `media_url` varchar(255) NOT NULL,
  `schedule_date` varchar(255) NOT NULL,
  `is_posted` tinyint(1) NOT NULL DEFAULT '0',
  `platform_id` int(11) NOT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cover_photo`
--

INSERT INTO `cover_photo` (`id`, `title`, `page_index`, `media_url`, `schedule_date`, `is_posted`, `platform_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, 'dasdsa', 'other', 'http://s3.amazonaws.com/sortechs/eremnews/cover_photo_1472737297.jpg', '2016-09-01 16:55', 1, 2, '2016-09-01 17:57:38', '2016-09-01 14:57:57', 1, NULL),
(4, 'hhh', 'main', 'http://s3.amazonaws.com/sortechs/eremnews/cover_photo_1472738425.png', '2016-09-01 16:45', 1, 1, '2016-09-01 18:01:51', '2016-09-01 15:02:00', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `day`
--

CREATE TABLE `day` (
  `id` int(11) NOT NULL,
  `title` enum('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `day`
--

INSERT INTO `day` (`id`, `title`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Sunday', 1, 1, '2016-08-03 17:36:06', '2016-08-03 17:40:47'),
(2, 'Monday', 1, NULL, '2016-08-03 17:36:17', NULL),
(3, 'Tuesday', 1, NULL, '2016-08-03 17:36:36', NULL),
(4, 'Wednesday', 1, NULL, '2016-08-03 17:36:38', NULL),
(5, 'Thursday', 1, NULL, '2016-08-03 17:36:41', NULL),
(6, 'Friday', 1, NULL, '2016-08-03 17:36:44', NULL),
(7, 'Saturday', 1, NULL, '2016-08-03 17:36:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `days_settings`
--

CREATE TABLE `days_settings` (
  `id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `platform_category_settings_id` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filler_data`
--

CREATE TABLE `filler_data` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `media_url` varchar(255) DEFAULT NULL,
  `type` enum('Text','Image','Preview','Video','Carousel','Albums','Multiple_Image') NOT NULL,
  `platform_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `publish_time` time NOT NULL,
  `last_generated` date DEFAULT NULL,
  `catch` tinyint(1) DEFAULT '0',
  `call_to_action` varchar(255) DEFAULT NULL,
  `type_call_to_action` enum('SHOP_NOW','BOOK_TRAVEL','LEARN_MORE','SIGN_UP','DOWNLOAD','WATCH_MORE','NO_BUTTON','INSTALL_MOBILE_APP','USE_MOBILE_APP','INSTALL_APP','USE_APP','PLAY_GAME','WATCH_VIDEO','OPEN_LINK','LISTEN_MUSIC','MOBILE_DOWNLOAD','GET_OFFER','GET_OFFER_VIEW','BUY_NOW','BUY_TICKETS','UPDATE_APP','BET_NOW','GET_DIRECTIONS','ADD_TO_CART','ORDER_NOW','SELL_NOW') DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hashtag`
--

CREATE TABLE `hashtag` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `media_url` varchar(255) DEFAULT NULL,
  `type` enum('Text','Image','Preview','Video','Carousel','Albums','Multiple_Image') NOT NULL,
  `platform_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `publish_time` time NOT NULL,
  `last_generated` date DEFAULT NULL,
  `catch` tinyint(1) DEFAULT '0',
  `call_to_action` varchar(255) DEFAULT NULL,
  `type_call_to_action` enum('SHOP_NOW','BOOK_TRAVEL','LEARN_MORE','SIGN_UP','DOWNLOAD','WATCH_MORE','NO_BUTTON','INSTALL_MOBILE_APP','USE_MOBILE_APP','INSTALL_APP','USE_APP','PLAY_GAME','WATCH_VIDEO','OPEN_LINK','LISTEN_MUSIC','MOBILE_DOWNLOAD','GET_OFFER','GET_OFFER_VIEW','BUY_NOW','BUY_TICKETS','UPDATE_APP','BET_NOW','GET_DIRECTIONS','ADD_TO_CART','ORDER_NOW','SELL_NOW') DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `row_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_host` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `media_news`
--

CREATE TABLE `media_news` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `type` enum('image','text','video','gallery') CHARACTER SET utf16 NOT NULL,
  `media` varchar(255) CHARACTER SET utf16 NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `creator` varchar(255) CHARACTER SET utf16 DEFAULT NULL,
  `column` varchar(255) CHARACTER SET utf16 DEFAULT NULL,
  `num_read` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) CHARACTER SET utf16 NOT NULL,
  `link` text CHARACTER SET utf16 NOT NULL,
  `link_md5` varchar(225) CHARACTER SET utf16 NOT NULL,
  `description` text CHARACTER SET utf16,
  `body_description` text CHARACTER SET utf16,
  `shorten_url` varchar(255) CHARACTER SET utf16 NOT NULL,
  `publishing_date` datetime NOT NULL,
  `schedule_date` datetime NOT NULL,
  `generated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page_details`
--

CREATE TABLE `page_details` (
  `id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `page_type_id` int(11) NOT NULL,
  `predication` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `page_details`
--

INSERT INTO `page_details` (`id`, `source_id`, `page_type_id`, `predication`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 1, 1, 'div[itemprop=author] span[itemprop=name]', '2016-08-31 14:12:00', NULL, 1, NULL),
(2, 2, 1, 'div[itemprop=author] span[itemprop=name]', '2016-08-31 14:12:00', NULL, 1, NULL),
(3, 3, 1, 'div[itemprop=author] span[itemprop=name]', '2016-08-31 14:12:01', NULL, 1, NULL),
(4, 4, 1, 'div[itemprop=author] span[itemprop=name]', '2016-08-31 14:12:01', NULL, 1, NULL),
(5, 5, 1, 'div[itemprop=author] span[itemprop=name]', '2016-08-31 14:12:01', NULL, 1, NULL),
(6, 6, 1, 'div[itemprop=author] span[itemprop=name]', '2016-08-31 14:12:01', NULL, 1, NULL),
(7, 7, 1, 'div[itemprop=author] span[itemprop=name]', '2016-08-31 14:12:01', NULL, 1, NULL),
(8, 8, 1, 'div[itemprop=author] span[itemprop=name]', '2016-08-31 14:12:01', NULL, 1, NULL),
(9, 1, 4, 'meta[property=og:title]', '2016-08-31 14:13:44', NULL, 1, NULL),
(10, 2, 4, 'meta[property=og:title]', '2016-08-31 14:13:44', NULL, 1, NULL),
(11, 3, 4, 'meta[property=og:title]', '2016-08-31 14:13:44', NULL, 1, NULL),
(12, 4, 4, 'meta[property=og:title]', '2016-08-31 14:13:45', NULL, 1, NULL),
(13, 5, 4, 'meta[property=og:title]', '2016-08-31 14:13:45', NULL, 1, NULL),
(14, 6, 4, 'meta[property=og:title]', '2016-08-31 14:13:45', NULL, 1, NULL),
(15, 7, 4, 'meta[property=og:title]', '2016-08-31 14:13:45', NULL, 1, NULL),
(16, 8, 4, 'meta[property=og:title]', '2016-08-31 14:13:45', NULL, 1, NULL),
(17, 1, 6, 'meta[name=twitter:description]', '2016-08-31 14:15:15', NULL, 1, NULL),
(18, 2, 6, 'meta[name=twitter:description]', '2016-08-31 14:15:15', NULL, 1, NULL),
(19, 3, 6, 'meta[name=twitter:description]', '2016-08-31 14:15:15', NULL, 1, NULL),
(20, 4, 6, 'meta[name=twitter:description]', '2016-08-31 14:15:15', NULL, 1, NULL),
(21, 5, 6, 'meta[name=twitter:description]', '2016-08-31 14:15:16', NULL, 1, NULL),
(22, 6, 6, 'meta[name=twitter:description]', '2016-08-31 14:15:16', NULL, 1, NULL),
(23, 7, 6, 'meta[name=twitter:description]', '2016-08-31 14:15:16', NULL, 1, NULL),
(24, 8, 6, 'meta[name=twitter:description]', '2016-08-31 14:15:16', NULL, 1, NULL),
(25, 1, 6, 'div[itemprop=articleBody] p span', '2016-08-31 14:17:09', NULL, 1, NULL),
(26, 3, 6, 'div[itemprop=articleBody] p span', '2016-08-31 14:17:09', NULL, 1, NULL),
(27, 4, 6, 'div[itemprop=articleBody] p span', '2016-08-31 14:17:09', NULL, 1, NULL),
(28, 6, 6, 'div[itemprop=articleBody] p span', '2016-08-31 14:17:09', NULL, 1, NULL),
(29, 7, 6, 'div[itemprop=articleBody] p span', '2016-08-31 14:17:09', NULL, 1, NULL),
(30, 8, 6, 'div[itemprop=articleBody] p span', '2016-08-31 14:17:09', NULL, 1, NULL),
(31, 1, 6, 'h4[itemprop=alternativeHeadline]', '2016-08-31 14:18:38', NULL, 1, NULL),
(32, 2, 6, 'h4[itemprop=alternativeHeadline]', '2016-08-31 14:18:38', NULL, 1, NULL),
(33, 3, 6, 'h4[itemprop=alternativeHeadline]', '2016-08-31 14:18:38', NULL, 1, NULL),
(34, 4, 6, 'h4[itemprop=alternativeHeadline]', '2016-08-31 14:18:38', NULL, 1, NULL),
(35, 5, 6, 'h4[itemprop=alternativeHeadline]', '2016-08-31 14:18:38', NULL, 1, NULL),
(36, 6, 6, 'h4[itemprop=alternativeHeadline]', '2016-08-31 14:18:39', NULL, 1, NULL),
(37, 7, 6, 'h4[itemprop=alternativeHeadline]', '2016-08-31 14:18:39', NULL, 1, NULL),
(38, 8, 6, 'h4[itemprop=alternativeHeadline]', '2016-08-31 14:18:39', NULL, 1, NULL),
(39, 1, 7, 'div[itemprop=articleBody] p ', '2016-08-31 14:20:45', NULL, 1, NULL),
(40, 2, 7, 'div[itemprop=articleBody] p ', '2016-08-31 14:20:45', NULL, 1, NULL),
(41, 3, 7, 'div[itemprop=articleBody] p ', '2016-08-31 14:20:46', NULL, 1, NULL),
(42, 4, 7, 'div[itemprop=articleBody] p ', '2016-08-31 14:20:46', NULL, 1, NULL),
(43, 5, 7, 'div[itemprop=articleBody] p ', '2016-08-31 14:20:46', NULL, 1, NULL),
(44, 6, 7, 'div[itemprop=articleBody] p ', '2016-08-31 14:20:46', NULL, 1, NULL),
(45, 7, 7, 'div[itemprop=articleBody] p ', '2016-08-31 14:20:46', NULL, 1, NULL),
(46, 8, 7, 'div[itemprop=articleBody] p ', '2016-08-31 14:20:46', NULL, 1, NULL),
(47, 1, 8, 'meta[property=article:published_time]', '2016-08-31 14:24:03', NULL, 1, NULL),
(48, 2, 8, 'meta[property=article:published_time]', '2016-08-31 14:24:03', NULL, 1, NULL),
(49, 3, 8, 'meta[property=article:published_time]', '2016-08-31 14:24:03', NULL, 1, NULL),
(50, 4, 8, 'meta[property=article:published_time]', '2016-08-31 14:24:03', NULL, 1, NULL),
(51, 5, 8, 'meta[property=article:published_time]', '2016-08-31 14:24:03', NULL, 1, NULL),
(52, 6, 8, 'meta[property=article:published_time]', '2016-08-31 14:24:03', NULL, 1, NULL),
(53, 7, 8, 'meta[property=article:published_time]', '2016-08-31 14:24:03', NULL, 1, NULL),
(54, 8, 8, 'meta[property=article:published_time]', '2016-08-31 14:24:03', NULL, 1, NULL),
(55, 1, 9, 'meta[property=og:image]', '2016-08-31 14:26:37', NULL, 1, NULL),
(56, 2, 9, 'meta[property=og:image]', '2016-08-31 14:26:37', NULL, 1, NULL),
(57, 3, 9, 'meta[property=og:image]', '2016-08-31 14:26:37', NULL, 1, NULL),
(58, 4, 9, 'meta[property=og:image]', '2016-08-31 14:26:37', NULL, 1, NULL),
(59, 5, 9, 'meta[property=og:image]', '2016-08-31 14:26:38', NULL, 1, NULL),
(60, 6, 9, 'meta[property=og:image]', '2016-08-31 14:26:38', NULL, 1, NULL),
(61, 7, 9, 'meta[property=og:image]', '2016-08-31 14:26:38', NULL, 1, NULL),
(62, 8, 9, 'meta[property=og:image]', '2016-08-31 14:26:38', NULL, 1, NULL),
(63, 1, 11, 'div[itemprop=articleBody] p img', '2016-08-31 14:45:55', NULL, 1, NULL),
(64, 2, 11, 'div[itemprop=articleBody] p img', '2016-08-31 14:45:55', NULL, 1, NULL),
(65, 3, 11, 'div[itemprop=articleBody] p img', '2016-08-31 14:45:55', NULL, 1, NULL),
(66, 4, 11, 'div[itemprop=articleBody] p img', '2016-08-31 14:45:55', NULL, 1, NULL),
(67, 5, 11, 'div[itemprop=articleBody] p img', '2016-08-31 14:45:55', NULL, 1, NULL),
(68, 6, 11, 'div[itemprop=articleBody] p img', '2016-08-31 14:45:56', NULL, 1, NULL),
(69, 7, 11, 'div[itemprop=articleBody] p img', '2016-08-31 14:45:56', NULL, 1, NULL),
(70, 8, 11, 'div[itemprop=articleBody] p img', '2016-08-31 14:45:56', NULL, 1, NULL),
(73, 1, 10, 'object[type=application/x-shockwave-flash]', '2016-08-31 15:19:44', NULL, 1, NULL),
(74, 2, 10, 'object[type=application/x-shockwave-flash]', '2016-08-31 15:19:44', NULL, 1, NULL),
(75, 8, 10, 'object[type=application/x-shockwave-flash]', '2016-08-31 15:19:45', NULL, 1, NULL),
(76, 1, 10, 'iframe', '2016-08-31 15:21:03', NULL, 1, NULL),
(77, 2, 10, 'iframe', '2016-08-31 15:21:03', NULL, 1, NULL),
(78, 3, 10, 'iframe', '2016-08-31 15:21:03', NULL, 1, NULL),
(79, 4, 10, 'iframe', '2016-08-31 15:21:03', NULL, 1, NULL),
(80, 5, 10, 'iframe', '2016-08-31 15:21:03', NULL, 1, NULL),
(81, 6, 10, 'iframe', '2016-08-31 15:21:03', NULL, 1, NULL),
(82, 7, 10, 'iframe', '2016-08-31 15:21:03', NULL, 1, NULL),
(83, 8, 10, 'iframe', '2016-08-31 15:21:03', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page_types`
--

CREATE TABLE `page_types` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `page_types`
--

INSERT INTO `page_types` (`id`, `title`, `created_at`) VALUES
(1, 'creator', '2016-08-11 04:21:40'),
(2, 'column', '2016-08-11 04:21:40'),
(3, 'num_read', '2016-08-11 04:21:55'),
(4, 'title', '2016-08-11 04:21:55'),
(5, 'link', '2016-08-11 04:22:15'),
(6, 'description', '2016-08-11 04:22:36'),
(7, 'body_description', '2016-08-11 04:22:36'),
(8, 'publishing_date', '2016-08-11 04:23:03'),
(9, 'image', '2016-08-11 04:23:03'),
(10, 'video', '2016-08-11 04:23:21'),
(11, 'gallery', '2016-08-11 04:23:21'),
(12, 'all_media', '2016-08-31 12:35:47');

-- --------------------------------------------------------

--
-- Table structure for table `pdf`
--

CREATE TABLE `pdf` (
  `id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `generated` tinyint(1) NOT NULL DEFAULT '0',
  `media_url` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `platform`
--

CREATE TABLE `platform` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `platform`
--

INSERT INTO `platform` (`id`, `title`, `deleted`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Facebook', 0, '2016-04-19 00:00:00', NULL, NULL, NULL),
(2, 'Twitter', 0, '2016-04-19 00:00:00', NULL, NULL, NULL),
(3, 'Instagram', 0, '2016-04-19 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `platform_category_settings`
--

CREATE TABLE `platform_category_settings` (
  `id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `platform_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post_queue`
--

CREATE TABLE `post_queue` (
  `id` int(11) NOT NULL,
  `post` text NOT NULL,
  `type` enum('Text','Image','Preview','Video','Carousel','Albums','Multiple_Image') NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `media_url` varchar(255) DEFAULT NULL,
  `settings` enum('general','custom') NOT NULL DEFAULT 'general',
  `schedule_date` datetime NOT NULL,
  `catgory_id` int(11) DEFAULT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `is_posted` tinyint(1) NOT NULL DEFAULT '0',
  `post_id` varchar(255) DEFAULT NULL,
  `news_id` int(11) DEFAULT NULL,
  `is_scheduled` tinyint(1) NOT NULL DEFAULT '0',
  `platform_id` int(11) NOT NULL,
  `generated` enum('auto','manual','clone','thematic','pinned') NOT NULL DEFAULT 'auto',
  `errors` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `call_to_action` varchar(255) DEFAULT NULL,
  `type_call_to_action` enum('SHOP_NOW','BOOK_TRAVEL','LEARN_MORE','SIGN_UP','DOWNLOAD','WATCH_MORE','NO_BUTTON','INSTALL_MOBILE_APP','USE_MOBILE_APP','INSTALL_APP','USE_APP','PLAY_GAME','WATCH_VIDEO','OPEN_LINK','LISTEN_MUSIC','MOBILE_DOWNLOAD','GET_OFFER','GET_OFFER_VIEW','BUY_NOW','BUY_TICKETS','UPDATE_APP','BET_NOW','GET_DIRECTIONS','ADD_TO_CART','ORDER_NOW','SELL_NOW') DEFAULT NULL,
  `pinned` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post_template`
--

CREATE TABLE `post_template` (
  `id` int(11) NOT NULL,
  `type` enum('Text','Image','Preview','Video','Carousel','Albums','Multiple_Image') NOT NULL DEFAULT 'Text',
  `catgory_id` int(11) DEFAULT NULL,
  `text` text NOT NULL,
  `platform_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_template`
--

INSERT INTO `post_template` (`id`, `type`, `catgory_id`, `text`, `platform_id`, `deleted`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(19, 'Preview', NULL, '[title] \r\n[author] \r\n[section]', 1, 0, '2016-07-12 17:22:05', '2016-09-01 12:35:50', 1, 1),
(20, 'Preview', NULL, '[title] \r\n[author] \r\n[section] \r\n[short_link]', 2, 0, '2016-07-12 17:22:44', '2016-09-01 12:35:59', 1, 1),
(21, 'Image', NULL, '[title] \r\n\r\n[description] \r\n\r\n[author] \r\n', 3, 0, '2016-07-12 17:23:04', '2016-09-01 12:36:04', 1, 1),
(24, 'Video', NULL, '[title] [description] \r\n[author] ', 1, 0, '2016-09-01 18:35:34', '2016-09-01 15:35:52', 1, 1),
(25, 'Video', NULL, '[title] \r\n', 2, 0, '2016-09-01 18:35:43', '2016-09-04 13:49:16', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile_pic`
--

CREATE TABLE `profile_pic` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `page_index` enum('main','other') DEFAULT NULL,
  `media_url` varchar(255) NOT NULL,
  `schedule_date` varchar(255) NOT NULL,
  `is_posted` tinyint(1) NOT NULL DEFAULT '0',
  `platform_id` int(11) NOT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `report_facebook`
--

CREATE TABLE `report_facebook` (
  `id` int(11) NOT NULL,
  `row_id` int(255) DEFAULT NULL,
  `subscribed` int(255) DEFAULT NULL,
  `likes` int(255) DEFAULT NULL,
  `shares` int(255) DEFAULT NULL,
  `comments` int(255) DEFAULT NULL,
  `impressions_unpaid` int(255) DEFAULT NULL,
  `impressions_paid` int(255) DEFAULT NULL,
  `post_consumptions` int(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `report_instagram`
--

CREATE TABLE `report_instagram` (
  `id` int(11) NOT NULL,
  `row_id` int(255) DEFAULT NULL,
  `comments` int(255) DEFAULT NULL,
  `likes` int(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `report_twitter`
--

CREATE TABLE `report_twitter` (
  `id` int(11) NOT NULL,
  `row_id` int(255) DEFAULT NULL,
  `retweet` int(255) DEFAULT NULL,
  `favorite` int(255) DEFAULT NULL,
  `reply` int(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `how_many_posts` int(11) DEFAULT NULL,
  `start_date_time` time NOT NULL,
  `end_date_time` time NOT NULL,
  `gap_time` int(11) NOT NULL DEFAULT '10',
  `updated_at` datetime DEFAULT NULL,
  `jobs_posts_per_day` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `jobs_posts_per_week` int(11) DEFAULT NULL,
  `date_check_job` date DEFAULT NULL,
  `timezone` varchar(255) NOT NULL,
  `direct_push` tinyint(1) NOT NULL DEFAULT '0',
  `direct_push_start` time NOT NULL,
  `direct_push_end` time NOT NULL,
  `generator_is_running` tinyint(1) DEFAULT NULL,
  `pinned` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `how_many_posts`, `start_date_time`, `end_date_time`, `gap_time`, `updated_at`, `jobs_posts_per_day`, `category_id`, `jobs_posts_per_week`, `date_check_job`, `timezone`, `direct_push`, `direct_push_start`, `direct_push_end`, `generator_is_running`, `pinned`, `created_at`, `created_by`, `updated_by`) VALUES
(1, 500, '00:00:00', '23:30:00', 10, '2016-08-31 18:25:23', 1, NULL, 1, '2016-07-24', 'Asia/Dubai', 0, '00:00:00', '00:00:00', 0, 0, '2016-07-24 10:22:11', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_obj`
--

CREATE TABLE `settings_obj` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `url_rss` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_migration`
--

CREATE TABLE `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1472646657),
('m160724_215332_init_tables', 1472646675),
('m160724_215733_new_fields', 1472646677),
('m160811_071053_category_level', 1472646940),
('m160811_071604_category_source', 1472646942),
('m160811_072357_page_type', 1472646945),
('m160811_072958_page_details', 1472646947),
('m160817_122512_time_settings', 1472646686),
('m160817_125653_news_update', 1472646689),
('m160817_170838_catsource', 1472646689),
('m160817_181611_all_media_column', 1472646947),
('m160818_064301_alterType', 1472646690),
('m160818_155924_cat_settings', 1472646691),
('m160822_163818_thematic_generated', 1472646691),
('m160824_111921_new_column', 1472646691);

-- --------------------------------------------------------

--
-- Table structure for table `time_settings`
--

CREATE TABLE `time_settings` (
  `id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `platform_category_settings_id` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `is_direct_push` tinyint(1) NOT NULL DEFAULT '0',
  `direct_push_start_time` time DEFAULT NULL,
  `direct_push_end_time` time DEFAULT NULL,
  `gap_time` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `gender` enum('m','f') NOT NULL,
  `background_color` varchar(255) NOT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `role` enum('admin','editor') NOT NULL DEFAULT 'editor',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `email`, `password`, `active`, `gender`, `background_color`, `profile_pic`, `role`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted`) VALUES
(1, 'admin', 'admin', 'ahmad.ghuneim@hotmail.com', '202cb962ac59075b964b07152d234b70', 1, 'm', 'skin-red', '', 'admin', '2016-07-04 23:19:20', '2016-07-24 21:46:05', NULL, 1, 0),
(2, 'ahmad', 'ahmad', 'ahmad@hotmail.com', '202cb962ac59075b964b07152d234b70', 1, 'm', 'skin-purple', NULL, 'admin', '2016-07-05 00:47:25', '2016-07-21 08:39:07', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `YiiSession`
--

CREATE TABLE `YiiSession` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `YiiSession`
--

INSERT INTO `YiiSession` (`id`, `expire`, `data`) VALUES
('gke7prd8pj98jstp4sjhf32nm2', 1472998471, 0x34376531323862623162613830393435316635616437333430653934373965635f5f72657475726e55726c7c733a32393a222f6572656d6e6577732f706f737451756575652f6d61696e2e68746d6c223b34376531323862623162613830393435316635616437333430653934373965635f5f69647c733a313a2231223b34376531323862623162613830393435316635616437333430653934373965635f5f6e616d657c733a353a2261646d696e223b34376531323862623162613830393435316635616437333430653934373965636e616d657c733a353a2261646d696e223b3437653132386262316261383039343531663561643733343065393437396563747970657c733a353a2261646d696e223b3437653132386262316261383039343531663561643733343065393437396563697341646d696e7c623a313b34376531323862623162613830393435316635616437333430653934373965635f5f7374617465737c613a333a7b733a343a226e616d65223b623a313b733a343a2274797065223b623a313b733a373a22697341646d696e223b623a313b7d6d61696e737c733a31343a22706f737451756575652f6d61696e223b706c6174666f726d7c613a333a7b693a313b733a383a2246616365626f6f6b223b693a323b733a373a2254776974746572223b693a333b733a393a22496e7374616772616d223b7d7363686564756c655f646174657c733a31303a22323031362d30392d3034223b746f7c733a31303a22323031362d30392d3035223b736f7274737c733a31333a227363686564756c655f64617465223b766965775f706f7374737c733a393a22616c6c5f706f737473223b747970657c733a353a22766964656f223b636174676f72795f69647c733a303a22223b69735f7363686564756c65647c693a313b556e5f706c6174666f726d7c613a333a7b693a313b733a383a2246616365626f6f6b223b693a323b733a373a2254776974746572223b693a333b733a393a22496e7374616772616d223b7d556e5f7363686564756c655f646174657c733a31303a22323031362d30392d3034223b556e5f746f7c733a31303a22323031362d30392d3035223b556e5f736f7274737c733a31333a227363686564756c655f64617465223b556e5f766965775f706f7374737c733a393a22616c6c5f706f737473223b556e5f747970657c733a303a22223b556e5f636174676f72795f69647c733a303a22223b556e5f69735f7363686564756c65647c693a303b);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `archives_media_news`
--
ALTER TABLE `archives_media_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_id` (`news_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `archives_news`
--
ALTER TABLE `archives_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_md5` (`link_md5`(191)),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `archives_post_queue`
--
ALTER TABLE `archives_post_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `category_level`
--
ALTER TABLE `category_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_source`
--
ALTER TABLE `category_source`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cover_photo`
--
ALTER TABLE `cover_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `platform_id` (`platform_id`),
  ADD KEY `platform_id_2` (`platform_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `day`
--
ALTER TABLE `day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `days_settings`
--
ALTER TABLE `days_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `platform_category_settings_id` (`platform_category_settings_id`),
  ADD KEY `day_id` (`day_id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `filler_data`
--
ALTER TABLE `filler_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `platform_id` (`platform_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `hashtag`
--
ALTER TABLE `hashtag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `platform_id` (`platform_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_news`
--
ALTER TABLE `media_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_id` (`news_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_md5` (`link_md5`(191)),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `sub_category_id` (`sub_category_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `page_details`
--
ALTER TABLE `page_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_types`
--
ALTER TABLE `page_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pdf`
--
ALTER TABLE `pdf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `platform`
--
ALTER TABLE `platform`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `platform_category_settings`
--
ALTER TABLE `platform_category_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `platform_id` (`platform_id`);

--
-- Indexes for table `post_queue`
--
ALTER TABLE `post_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catgory_id` (`catgory_id`),
  ADD KEY `catgory_id_2` (`catgory_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `platform_id` (`platform_id`),
  ADD KEY `news_id` (`news_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `post_template`
--
ALTER TABLE `post_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `platform_id` (`platform_id`),
  ADD KEY `catgory_id` (`catgory_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `profile_pic`
--
ALTER TABLE `profile_pic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `platform_id` (`platform_id`),
  ADD KEY `platform_id_2` (`platform_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `report_facebook`
--
ALTER TABLE `report_facebook`
  ADD PRIMARY KEY (`id`),
  ADD KEY `row_id` (`row_id`,`updated_by`,`created_by`);

--
-- Indexes for table `report_instagram`
--
ALTER TABLE `report_instagram`
  ADD PRIMARY KEY (`id`),
  ADD KEY `row_id` (`row_id`,`updated_by`,`created_by`);

--
-- Indexes for table `report_twitter`
--
ALTER TABLE `report_twitter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `row_id` (`row_id`,`updated_by`,`created_by`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `settings_obj`
--
ALTER TABLE `settings_obj`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `tbl_migration`
--
ALTER TABLE `tbl_migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `time_settings`
--
ALTER TABLE `time_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`,`updated_by`);

--
-- Indexes for table `YiiSession`
--
ALTER TABLE `YiiSession`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `archives_media_news`
--
ALTER TABLE `archives_media_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `archives_news`
--
ALTER TABLE `archives_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `archives_post_queue`
--
ALTER TABLE `archives_post_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `category_level`
--
ALTER TABLE `category_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `category_source`
--
ALTER TABLE `category_source`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cover_photo`
--
ALTER TABLE `cover_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `day`
--
ALTER TABLE `day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `days_settings`
--
ALTER TABLE `days_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `filler_data`
--
ALTER TABLE `filler_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hashtag`
--
ALTER TABLE `hashtag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `media_news`
--
ALTER TABLE `media_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2472;
--
-- AUTO_INCREMENT for table `page_details`
--
ALTER TABLE `page_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `page_types`
--
ALTER TABLE `page_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `pdf`
--
ALTER TABLE `pdf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `platform`
--
ALTER TABLE `platform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `platform_category_settings`
--
ALTER TABLE `platform_category_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post_queue`
--
ALTER TABLE `post_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7309;
--
-- AUTO_INCREMENT for table `post_template`
--
ALTER TABLE `post_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `profile_pic`
--
ALTER TABLE `profile_pic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `report_facebook`
--
ALTER TABLE `report_facebook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `report_instagram`
--
ALTER TABLE `report_instagram`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `report_twitter`
--
ALTER TABLE `report_twitter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings_obj`
--
ALTER TABLE `settings_obj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `time_settings`
--
ALTER TABLE `time_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cover_photo`
--
ALTER TABLE `cover_photo`
  ADD CONSTRAINT `cover_photo_ibfk_1` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `filler_data`
--
ALTER TABLE `filler_data`
  ADD CONSTRAINT `platform` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `news_ibfk_2` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `post_queue`
--
ALTER TABLE `post_queue`
  ADD CONSTRAINT `post_queue_ibfk_1` FOREIGN KEY (`catgory_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `post_queue_ibfk_2` FOREIGN KEY (`catgory_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `post_queue_ibfk_3` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `post_queue_ibfk_4` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `post_template`
--
ALTER TABLE `post_template`
  ADD CONSTRAINT `post_template_ibfk_1` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
